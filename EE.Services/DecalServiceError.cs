﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace EE.Services {
    public enum DecalServiceErrorCode {
        ERROR_SCRIPT_FILE_NOT_FOUND = 1,
        ERROR_SCRIPT_ALREADY_RUNNING = 2,
    }

    [DataContract]
    public class DecalServiceError {
        /// <summary>
        /// Gets or set the error code.
        /// </summary>
        [DataMember]
        public int code { get; set; }

        /// <summary>
        /// Gets or sets the error text.
        /// </summary>
        [DataMember]
        public string text { get; set; }

        public DecalServiceError(DecalServiceErrorCode errorCode, string errorText) {
            code = (int)errorCode;
            text = errorText;
        }
    }
}
