﻿using System.Collections.Generic;
using System.ServiceModel;

namespace EE.Services
{
    /// <summary>
    /// Callback definition for clients.
    /// </summary>
    public interface IDecalServiceCallback {
        /// <summary>
        /// ping client
        /// </summary>
        [OperationContract(IsOneWay = true)]
        void Ping();

        /// <summary>
        /// send error up to client
        /// </summary>
        [OperationContract(IsOneWay = true)]
        void Error(DecalServiceError error);

        /// <summary>
        /// Add text to the client's chat window
        /// </summary>
        /// <param name="text"></param>
        /// <param name="color"></param>
        [OperationContract(IsOneWay = true)]
        void AddChatText(string text, int color);

        /// <summary>
        /// Add a command to the client's chat box
        /// </summary>
        /// <param name="command"></param>
        [OperationContract(IsOneWay = true)]
        void InvokeChatParser(string command);

        /// <summary>
        /// let the client know it is registered, provide it with a client id
        /// </summary>
        /// <param name="uid"></param>
        [OperationContract(IsOneWay = true)]
        void Registered(string uid);

        /// <summary>
        /// let the client know a new script is running
        /// </summary>
        /// <param name="sid">script id</param>
        /// <param name="scriptName">script name</param>
        [OperationContract(IsOneWay = true)]
        void NewScriptRunning(string sid, string scriptName);

        /// <summary>
        /// let the client know a script has stopped running
        /// </summary>
        /// <param name="sid">script id</param>
        /// <param name="sid">script name</param>
        [OperationContract(IsOneWay = true)]
        void ScriptStopped(string sid, string scriptName);

        /// <summary>
        /// let the client know what scripts are available to run
        /// </summary>
        /// <param name="scripts">list of available scripts</param>
        [OperationContract(IsOneWay = true)]
        void AvailableScripts(List<string> scripts);
    }
}
