﻿using EE.Services.EventArgs;
using System.ServiceModel;

namespace EE.Services {
    /// <summary>
    /// Service contract for the product service.
    /// </summary>
    [ServiceContract(SessionMode = SessionMode.Required,
        CallbackContract = typeof(IDecalServiceCallback))]
    public interface IDecalService {
        /// <summary>
        /// Register a client with the service host
        /// <param name="serverName">the name of the server the client is currently logged in to</param>
        /// <param name="characterName">the name of the character the account is currently logged in as</param>
        /// </summary>
        [OperationContract]
        void RegisterClient(string serverName, string characterName);

        /// <summary>
        /// unregister the client with host, this will shutdown all running character scripts
        /// </summary>
        [OperationContract]
        void UnregisterClient();

        /// <summary>
        /// respond to a ping from ee host
        /// </summary>
        [OperationContract]
        void Pong();

        /// <summary>
        /// Run a client script on the service host
        /// <param name="scriptName">the name of the script to run</param>
        /// </summary>
        [OperationContract]
        void RunClientScript(string scriptName);

        /// <summary>
        /// Stop a running script on the service host
        /// <param name="sid">the sid of the script to stop</param>
        /// </summary>
        [OperationContract]
        void StopClientScript(string sid);

        /// <summary>
        /// Request a list of available scripts from the host
        /// </summary>
        [OperationContract]
        void RequestAvailableScripts();

        // decal stuff

        /// <summary>
        /// Notify the host when a ChatBoxMessageEvent takes place
        /// </summary>
        /// <param name="text">the text of the chatbox message</param>
        /// <param name="target">target ?</param>
        /// <param name="color">color of the message</param>
        [OperationContract]
        void OnChatBoxMessage(ChatTextInterceptEventArgsWrapper e);

        /// <summary>
        /// Notify the host when a ChatClickInterceptEvent takes place
        /// </summary>
        /// <param name="text">the text of the chat name that was clicked</param>
        /// <param name="id">id ?</param>
        [OperationContract]
        void OnChatNameClicked(ChatClickInterceptEventArgsWrapper e);

        /// <summary>
        /// Notify the host when a ChatParserInterceptEvent takes place
        /// </summary>
        /// <param name="text">the text of the chat message that was intercepted</param>
        /// <param name="id">id ?</param>
        [OperationContract]
        void OnCommandLineText(ChatParserInterceptEventArgsWrapper e);

        /// <summary>
        /// Notify the host when a ContainerOpenedEvent takes place
        /// </summary>
        /// <param name="id">id of the container that was opened</param>
        [OperationContract]
        void OnContainerOpened(ContainerOpenedEventArgsWrapper e);

        /// <summary>
        /// Notify the host when a ChangeFellowship takes place
        /// </summary>
        /// <param name="id">id of the character? that the change references</param>
        /// <param name="type">type of change, see FellowshipEventType</param>
        [OperationContract]
        void OnChangeFellowship(ChangeFellowshipEventArgsWrapper e);

        /// <summary>
        /// Notify the host when an object changes takes place
        /// </summary>
        /// <param name="changeType">type of object change</param>
        /// <param name="eewo">EEWorldObject of the changed object</param>
        [OperationContract]
        void OnChangeObject(ChangeObjectEventArgsWrapper e);

        /// <summary>
        /// Notify the host when an object is created
        /// </summary>
        /// <param name="eewo">EEWorldObject of the created object</param>
        [OperationContract]
        void OnCreateObject(CreateObjectEventArgsWrapper e);

        /// <summary>
        /// Notify the host when an object is released
        /// </summary>
        /// <param name="eewo">EEWorldObject of the released object</param>
        [OperationContract]
        void OnReleaseObject(ReleaseObjectEventArgsWrapper e);

        /// <summary>
        /// Notify the host when a character has completely logged in
        /// </summary>
        [OperationContract]
        void OnLoginComplete();

        /// <summary>
        /// Notify the host the character has changed in some way
        /// </summary>
        /// <param name="eec">EECharacter</param>
        [OperationContract]
        void OnChangeCharacter(EECharacter eec);

        /// <summary>
        /// Notify the host the character's vital has changed
        /// </summary>
        /// <param name="amount">new amount?</param>
        /// <param name="type">vital type</param>
        [OperationContract]
        void OnChangeVital(ChangeVitalEventArgsWrapper e);
    }
}
