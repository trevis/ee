﻿using Decal.Adapter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace EE.Services.EventArgs {
    [DataContract]
    public class ChatParserInterceptEventArgsWrapper {
        /// <summary>
        /// Gets or set the text.
        /// </summary>
        [DataMember]
        public string text { get; set; }

        public ChatParserInterceptEventArgsWrapper(string _text) {
            text = _text;
        }

        public ChatParserInterceptEventArgsWrapper(ChatParserInterceptEventArgs e) {
            text = e.Text;
        }
    }
}
