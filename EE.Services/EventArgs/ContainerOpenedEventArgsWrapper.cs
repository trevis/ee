﻿using Decal.Adapter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace EE.Services.EventArgs {
    [DataContract]
    public class ContainerOpenedEventArgsWrapper {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [DataMember]
        public int id { get; set; }

        public ContainerOpenedEventArgsWrapper(int _id) {
            id = _id;
        }

        public ContainerOpenedEventArgsWrapper(ContainerOpenedEventArgs e) {
            id = e.ItemGuid;
        }
    }
}
