﻿using Decal.Adapter;
using Decal.Adapter.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace EE.Services.EventArgs {
    [DataContract]
    public class ReleaseObjectEventArgsWrapper {
        /// <summary>
        /// Gets or sets the world object
        /// </summary>
        [DataMember]
        public EEWorldObject wo { get; set; }

        public ReleaseObjectEventArgsWrapper(EEWorldObject _wo) {
            wo = _wo;
        }
    }
}
