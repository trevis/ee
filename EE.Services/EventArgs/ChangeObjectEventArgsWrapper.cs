﻿using Decal.Adapter;
using Decal.Adapter.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace EE.Services.EventArgs {
    [DataContract]
    public class ChangeObjectEventArgsWrapper {
        /// <summary>
        /// Gets or set the change type
        /// </summary>
        [DataMember]
        public int type { get; set; }

        /// <summary>
        /// Gets or sets the world object
        /// </summary>
        [DataMember]
        public EEWorldObject wo { get; set; }

        public ChangeObjectEventArgsWrapper(int changeType, EEWorldObject _wo) {
            type = changeType;
            wo = _wo;
        }
    }
}
