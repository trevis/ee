﻿using Decal.Adapter;
using Decal.Adapter.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace EE.Services.EventArgs {
    [DataContract]
    public class ChangeVitalEventArgsWrapper {
        /// <summary>
        /// Gets or set the text.
        /// </summary>
        [DataMember]
        public int type { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [DataMember]
        public int amount { get; set; }

        public ChangeVitalEventArgsWrapper(int _amount, int _type) {
            amount = _amount;
            type = _type;
        }

        public ChangeVitalEventArgsWrapper(ChangeVitalEventArgs e) {
            amount = e.Amount;
            type = (int)e.Type;
        }
    }
}
