﻿using Decal.Adapter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace EE.Services.EventArgs {
    [DataContract]
    public class ChatClickInterceptEventArgsWrapper {
        /// <summary>
        /// Gets or set the text.
        /// </summary>
        [DataMember]
        public string text { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [DataMember]
        public int id { get; set; }

        public ChatClickInterceptEventArgsWrapper(string _text, int _id) {
            text = _text;
            id = _id;
        }

        public ChatClickInterceptEventArgsWrapper(ChatClickInterceptEventArgs e) {
            text = e.Text;
            id = e.Id;
        }
    }
}
