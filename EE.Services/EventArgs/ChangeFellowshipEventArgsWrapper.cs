﻿using Decal.Adapter;
using Decal.Adapter.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace EE.Services.EventArgs {
    [DataContract]
    public class ChangeFellowshipEventArgsWrapper {
        /// <summary>
        /// Gets or set the text.
        /// </summary>
        [DataMember]
        public int type { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [DataMember]
        public int id { get; set; }

        public ChangeFellowshipEventArgsWrapper(int _id, int _type) {
            id = _id;
            type = _type;
        }

        public ChangeFellowshipEventArgsWrapper(ChangeFellowshipEventArgs e) {
            id = e.Id;
            type = (int)e.Type;
        }
    }
}
