﻿using Decal.Adapter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace EE.Services.EventArgs {
    [DataContract]
    public class ChatTextInterceptEventArgsWrapper {
        /// <summary>
        /// Gets or set the text.
        /// </summary>
        [DataMember]
        public string text { get; set; }

        /// <summary>
        /// Gets or sets the target.
        /// </summary>
        [DataMember]
        public int target { get; set; }

        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        [DataMember]
        public int color { get; set; }

        public ChatTextInterceptEventArgsWrapper(string _text, int _color, int _target) {
            text = _text;
            color = _color;
            target = _target;
        }

        public ChatTextInterceptEventArgsWrapper(ChatTextInterceptEventArgs e) {
            text = e.Text;
            color = e.Color;
            target = e.Target;
        }
    }
}
