﻿using Decal.Adapter;
using Decal.Adapter.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace EE.Services.EventArgs {
    [DataContract]
    public class CreateObjectEventArgsWrapper {
        /// <summary>
        /// Gets or sets the world object
        /// </summary>
        [DataMember]
        public EEWorldObject wo { get; set; }

        public CreateObjectEventArgsWrapper(EEWorldObject worldObject) {
            wo = worldObject;
        }
    }
}
