﻿using Decal.Adapter.Wrappers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace EE.Services {
    // this represents your own character
    [DataContract]
    public class EECharacter : INotifyPropertyChanged {
        [DataMember]
        private int id;
        public int Id {
            get { return id; }
            set { SetField(ref id, value, "Id"); }
        }

        [DataMember]
        private string name;
        public string Name {
            get { return name; }
            set { SetField(ref name, value, "Name"); }
        }

        [DataMember]
        private int health;
        public int Health {
            get { return health; }
            set { SetField(ref health, value, "Health"); }
        }

        [DataMember]
        private int mana;
        public int Mana {
            get { return mana; }
            set { SetField(ref mana, value, "Mana"); }
        }

        [DataMember]
        private int stamina;
        public int Stamina {
            get { return stamina; }
            set { SetField(ref stamina, value, "Stamina"); }
        }

        [DataMember]
        private int effectiveHealth;
        public int EffectiveHealth {
            get { return effectiveHealth; }
            set { SetField(ref effectiveHealth, value, "EffectiveHealth"); }
        }

        [DataMember]
        private int effectiveStamina;
        public int EffectiveStamina {
            get { return effectiveStamina; }
            set { SetField(ref effectiveStamina, value, "EffectiveStamina"); }
        }

        [DataMember]
        private int effectiveMana;
        public int EffectiveMana {
            get { return effectiveMana; }
            set { SetField(ref effectiveMana, value, "EffectiveMana"); }
        }

        public bool IsDirty { get; private set; }
        public event PropertyChangedEventHandler PropertyChanged;

        public EECharacter(int id) {
            this.id = id;
        }

        public EECharacter(CharacterFilter cf) {
            id = cf.Id;
            UpdatePropertiesFromCharacterFilter(cf);
        }

        public void UpdatePropertiesFromCharacterFilter(CharacterFilter cf) {
            Name = cf.Name;

            Health = cf.Health;
            Stamina = cf.Stamina;
            Mana = cf.Mana;

            EffectiveHealth = cf.EffectiveVital[CharFilterVitalType.Health];
            EffectiveStamina = cf.EffectiveVital[CharFilterVitalType.Stamina];
            EffectiveMana = cf.EffectiveVital[CharFilterVitalType.Mana];
        }

        public void Clean() {
            IsDirty = false;
        }

        protected void SetField<T>(ref T field, T value, string propertyName) {
            if (!EqualityComparer<T>.Default.Equals(field, value)) {
                field = value;
                IsDirty = true;
                OnPropertyChanged(propertyName);
            }
        }

        protected virtual void OnPropertyChanged(string propertyName) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
