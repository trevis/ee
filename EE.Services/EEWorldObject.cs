﻿using Decal.Adapter.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace EE.Services {
    [DataContract]
    public class EEWorldObject {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [DataMember]
        public int id { get; set; }

        /// <summary>
        /// spellCount
        /// </summary>
        [DataMember]
        public int spellCount { get; set; }

        /// <summary>
        /// behavior
        /// </summary>
        [DataMember]
        public int behavior { get; set; }

        /// <summary>
        /// category
        /// </summary>
        [DataMember]
        public int category { get; set; }

        /// <summary>
        /// container
        /// </summary>
        [DataMember]
        public int container { get; set; }

        /// <summary>
        /// gameDataFlags1
        /// </summary>
        [DataMember]
        public int gameDataFlags1 { get; set; }

        /// <summary>
        /// hasIdData
        /// </summary>
        [DataMember]
        public bool hasIdData { get; set; }

        /// <summary>
        /// icon
        /// </summary>
        [DataMember]
        public int icon { get; set; }

        /// <summary>
        /// lastIdTime
        /// </summary>
        [DataMember]
        public int lastIdTime { get; set; }

        /// <summary>
        /// name
        /// </summary>
        [DataMember]
        public string name { get; set; }

        /// <summary>
        /// objectClass
        /// </summary>
        [DataMember]
        public int objectClass { get; set; }

        /// <summary>
        /// type
        /// </summary>
        [DataMember]
        public int type { get; set; }

        /// <summary>
        /// holds all boolValues
        /// </summary>
        [DataMember]
        public Dictionary<int, bool> boolValues = new Dictionary<int, bool>();

        /// <summary>
        /// holds all doubleValues
        /// </summary>
        [DataMember]
        public Dictionary<int, double> doubleValues = new Dictionary<int, double>();

        /// <summary>
        /// holds all longValues
        /// </summary>
        [DataMember]
        public Dictionary<int, long> longValues = new Dictionary<int, long>();

        /// <summary>
        /// holds all stringValues
        /// </summary>
        [DataMember]
        public Dictionary<int, string> stringValues = new Dictionary<int, string>();


        public EEWorldObject(int _id) {
            id = _id;
        }

        public EEWorldObject(WorldObject wo) {
            id = wo.Id;
            UpdatePropertiesFromWorldObject(wo);
        }

        public void UpdatePropertiesFromWorldObject(WorldObject wo) {
            id = wo.Id;
            spellCount = wo.SpellCount;
            behavior = wo.Behavior;
            category = wo.Category;
            container = wo.Container;
            gameDataFlags1 = wo.GameDataFlags1;
            hasIdData = wo.HasIdData;
            icon = wo.Icon;
            lastIdTime = wo.LastIdTime;
            name = wo.Name;
            objectClass = (int)wo.ObjectClass;
            type = wo.Type;

            foreach (int key in wo.BoolKeys) {
                var value = wo.Values((BoolValueKey)key);

                if (boolValues.ContainsKey(key)) {
                    boolValues[key] = value;
                }
                else {
                    boolValues.Add(key, value);
                }
            }

            foreach (int key in wo.DoubleKeys) {
                var value = wo.Values((DoubleValueKey)key);

                if (doubleValues.ContainsKey(key)) {
                    doubleValues[key] = value;
                }
                else {
                    doubleValues.Add(key, value);
                }
            }

            foreach (int key in wo.LongKeys) {
                var value = wo.Values((LongValueKey)key);

                if (longValues.ContainsKey(key)) {
                    longValues[key] = value;
                }
                else {
                    longValues.Add(key, value);
                }
            }

            foreach (int key in wo.StringKeys) {
                var value = wo.Values((StringValueKey)key);

                if (stringValues.ContainsKey(key)) {
                    stringValues[key] = value;
                }
                else {
                    stringValues.Add(key, value);
                }
            }
        }
    }
}
