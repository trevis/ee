﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace EE.ConsoleClient {
    class Program {
        static void Main(string[] args) {
            ClientController client = new ClientController();

            Console.WriteLine("Init Client");
            client.Init();

            while (true) {
                Console.WriteLine("Press enter to send chat message event");
                var name = Console.ReadLine();
                if (name.Length > 0) {
                    client.StartScript(name);
                }
                else {
                    if (client.pipeProxy != null) {
                        client.pipeProxy.RequestAvailableScripts();
                    }
                }
            }
        }
    }
}
