﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Configuration;
using System.Text;
using System.Threading;
using EE.Services;

namespace EE.ConsoleClient {
    /// <summary>
    /// Client controller. Acts as callback client.
    /// </summary>
    public class ClientController : IDecalServiceCallback {
        public IDecalService pipeProxy;
        private ChannelFactory<IDecalService> pipeFactory;

        private readonly System.Timers.Timer checkConnectionTimer;
        private readonly System.Timers.Timer pingTimer;
        private string uid;

        /// <summary>
        /// Creates a new instance of the <see cref="ClientController"/> class.
        /// </summary>
        public ClientController() {
            //init connection timer
            this.checkConnectionTimer = new System.Timers.Timer(5000);
            this.checkConnectionTimer.Elapsed += CheckConnectionTimerElapsed;
            
            this.pingTimer = new System.Timers.Timer(1000);
            this.checkConnectionTimer.Elapsed += CheckPingTimer;
        }

        public void Init() {
            Thread thread = new Thread(Connect);
            thread.Start();
        }

        private bool IsPipeOpen() {
            return (pipeProxy != null && ((IClientChannel)pipeProxy).State == CommunicationState.Opened);
        }

        private void Connect() {
            Console.WriteLine("Attempting connect");
            //get endpoint configuration from app.config.
            var group =
                ServiceModelSectionGroup.GetSectionGroup(
                ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None));

            if (group != null) {
                //create duplex channel factory
                //pipeFactory = new DuplexChannelFactory<IDecalService>(this, group.Client.Endpoints[0].Name);
                var binding = new NetNamedPipeBinding();

                binding.OpenTimeout = TimeSpan.FromSeconds(1);
                binding.SendTimeout = TimeSpan.FromSeconds(1);
                binding.ReceiveTimeout = TimeSpan.FromSeconds(1);

                pipeFactory = new DuplexChannelFactory<IDecalService>(
                        new InstanceContext(this),
                        binding,
                        "net.pipe://localhost/EEHostService");
               

                //create a communication channel and register for its events
                pipeProxy = pipeFactory.CreateChannel();
                ((IClientChannel)pipeProxy).Opened += PipeProxyOpened;

                try {
                    //try to open the connection
                    ((IClientChannel)pipeProxy).Open();
                    this.checkConnectionTimer.Stop();

                    //register 
                    if (IsPipeOpen()) {
                        pipeProxy.RegisterClient("Console", "Test");
                    }
                }
                catch (Exception e) {
                    //for example show status text or log; 
                    Console.WriteLine(e.Message);
                }
            }
            else {
                Console.WriteLine("Unable to read endpoint config");
            }
        }

        void CheckConnectionTimerElapsed(object sender, System.Timers.ElapsedEventArgs e) {
            //if the channel is not already open, call Initialize
            if (pipeProxy == null || ((IClientChannel)pipeProxy).State != CommunicationState.Opened
                || ((IClientChannel)pipeProxy).State != CommunicationState.Opening) {
                Connect();
            }
        }

        void CheckPingTimer(object sender, System.Timers.ElapsedEventArgs e) {
            try {
                if (IsPipeOpen()) {
                    pipeProxy.Pong();
                }
            }
            catch (Exception ex) {  }
        }

        void PipeProxyOpened(object sender, EventArgs e) {
            this.checkConnectionTimer.Stop();
            this.pingTimer.Start();

            ((IClientChannel)pipeProxy).Faulted += PipeProxyFaulted;

            Console.WriteLine("Connected to host.");
        }

        void PipeProxyFaulted(object sender, EventArgs e) {
            var proxy = sender as IClientChannel;

            Console.WriteLine("Disconnected from host.");

            if (proxy != null) {
                proxy.Faulted -= PipeProxyFaulted;
                proxy.Opened -= PipeProxyOpened;
                proxy = null;
            }

            this.pipeFactory = null;
            this.checkConnectionTimer.Start();
            this.pingTimer.Stop();
        }


        /// <summary>
        /// Sends a ChatMessageEvent to the server
        /// </summary>
        /// <param name="product"></param>
        public void StartScript(string name) {
            try {
                if (IsPipeOpen()) {
                    pipeProxy.RunClientScript(name);
                }
                else {
                    Console.WriteLine("Not connected to host.");
                }
            }
            catch (Exception e) {
                Console.WriteLine(e);
            }
        }

        /// <summary>
        /// Invoked when host has registered us
        /// </summary>
        /// <param name="uid"></param>
        public void Registered(string uid) {
            this.uid = uid;
            Console.WriteLine("We have been registered: " + uid);
        }

        public void Ping() {
            var thread = new Thread(new ThreadStart(Pong));
            thread.Start();
        }

        public void Error(DecalServiceError error) {
            //error.code;
            //error.text;
            Console.WriteLine(String.Format("Error: {0}", error.text));
        }

        void Pong() {
            try {
                if (IsPipeOpen()) {
                    pipeProxy.Pong();
                }
                else {
                    Console.WriteLine("Not connected to host.");
                }
            }
            catch (Exception e) { }
        }

        /// <summary>
        /// Invoked when host has registered us
        /// </summary>
        /// <param name="uid"></param>
        public void NewScriptRunning(string sid, string scriptName) {
            Console.WriteLine(String.Format("script is running: {0}.{1}", scriptName, sid));
        }

        public void ScriptStopped(string sid, string scriptName) {
            Console.WriteLine(String.Format("script was stopped: {0}.{1}", scriptName, sid));
        }

        public void AvailableScripts(List<string> scripts) {
            foreach (var script in scripts) {
                Console.WriteLine(string.Format("Available script: {0}", script));
            }
        }

        public void InvokeChatParser(string command) {
            Console.WriteLine(String.Format("InvokeChatParser: {0}", command));
        }

        /// <summary>
        /// Invoked when host wants to add text to the chat window
        /// </summary>
        /// <param name="product"></param>
        public void AddChatText(string text, int color) {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append(text);
            //stringBuilder.Append("   - color:");
            //stringBuilder.Append(color.ToString());
            Console.WriteLine(stringBuilder);
        }
    }
}
