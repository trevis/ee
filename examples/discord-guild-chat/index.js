const Discord = require('discord.js');
const striptags = require('striptags');

const ExpressionEngineScript = require('./ee');

// check out this page for instructions on getting a channelId and loginToken
// https://github.com/Chikachi/DiscordIntegration/wiki/How-to-get-a-token-and-channel-ID-for-Discord
// https://discordapp.com/developers/applications/me


// edit this config to use your own discord info / character name
const config = {
    loginToken: "your token for your discord bot", // change this to the login token for discord bot
    channel: "535267517971562516", // change this to your own channel id
    characterName: "Gdiscord" // change this to the AC character name running this script
};

// Create an instance of a Discord client
const discord = new Discord.Client();

// create an instance of an ee script
// note: this is required unless you really know what you are doing.
const ee = new ExpressionEngineScript();

// handle discord ready/connected event
discord.on('ready', () => {
    ee.log('connected to discord');
});

// handle messages from the AC client so we can forward to discord
ee.on("ChatBoxMessage", (event) => {
    // we only want allegiance messages
    if (event.text.startsWith("[Allegiance]")) {
        // find the discord channel defined in our config
        const channel = discord.channels.find(ch => ch.id === config.channel);

        // remove the [Allegiance] prefix from the message
        let newMessage = event.text.replace("[Allegiance] ", "");

        // if this is a message from the character running the bot, bail out
        if (newMessage.startsWith(config.characterName + " says")) return;

        // send our modified message to the discord channel if it exists
        if (channel) channel.send(newMessage);
    }
});

// Create an event listener for discord messages so we can forward to AC
discord.on('message', message => {
    // if this didnt happen in the channel we are monitoring bail out
    if (message.channel.id !== config.channel) return;

    // if this is a bot dont dont forward the message
    if (message.author.bot) return;

    // this is the discord username of the person who posted the message
    let username = message.author.username;

    // get the message contents, but remove any newline chars
    let newMessage = message.content.replace(/(\r|\n)/gm, "");

    // if the user has a nickname set, use that instead
    if (message.member && message.member.nickname) {
        username = message.member.nickname;
    }

    // strip out any html-like tags from the message
    newMessage = striptags(newMessage);

    // if the message is empty now we shouldn't forward it
    if (newMessage.length == 0) return;

    // format the message and limit to 180 chars
    newMessage = `${username} says, ${newMessage}`.substr(0, 180);

    // send this to the chat parser of ac client running this script.
    // this acts like the client typed the message into the chat box.
    ee.invokeChatParser("/a " + newMessage);
});

// Log in our discord bot
discord.login(config.loginToken);

// scripts *must* return ee.register()
return ee.register();