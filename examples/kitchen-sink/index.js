const ExpressionEngineScript = require('ee');
const FellowshipEventType = require('ee/enums/fellowshipeventtype');
const WorldChangeType = require('ee/enums/worldchangetype');
const CharFilterVitalType = require('ee/enums/charfiltervitaltype');

// all scripts need to create a new instance of ExpressionEngineScript
const ee = new ExpressionEngineScript();

// hold our interval id
var intervalId;

// get an object key by value
function objectKeyFromValue(obj, value) {
    return Object.keys(obj).find(key => obj[key] === value);
}

// handle when text gets added to the chat window
// ex: [Allegiance] Sunnuj says, "Hello"
// note: currently there is no support to "eat" this event
//
// event.color is an int that represents the text color of the chat message
ee.on("ChatBoxMessage", function (event) {
    ee.log(`got ChatBoxMessage event: ${event.color} ${event.text}`);
});

// handle when a chat name is clicked.
// note: currently there is no support to "eat" this event
ee.on("ChatNameClicked", function (event) {
    ee.log(`got ChatNameClicked event: ${event.text}`);
});

// this is called when the user types something into the chatbox,
// like a command or a chat message.  Sometimes other plugin messages
// that were echo'd to the chat window will show up here.
//
// event.text is the text that was entered
ee.on("CommandLineText", function (event) {
    ee.log(`got CommandLineText event: ${event.text}`);
});

// called when a container is opened / closed.
//
// event.id is 0 when the container is closed, otherwise is the id of the container
ee.on("ContainerOpened", function (event) {
    ee.log(`got ContainerOpened event: ${event.id}`);
});

// handle fellowship change event, event.type is one of FellowshipEventType
ee.on("ChangeFellowship", function (event) {
    var changeType = objectKeyFromValue(FellowshipEventType, event.type);

    // if (event.type === FellowshipEventType.Disband)

    ee.log(`got ChangeFellowship event: id: ${event.id} type: ${changeType}`);
});

// this is fired when an object is changed (item info, mana change, etc)
ee.on("ChangeObject", (event) => {
    var changeType = objectKeyFromValue(WorldChangeType, event.type);

    // this can be super spammy
    //ee.log(`got ChangeObject: ${changeType} ${event.wo.name} (${event.wo.id})`);
});

// this is fired when the client becomes aware of a new object
ee.on("CreateObject", (event) => {
    // this can be super spammy
    //ee.log(`got CreateObject: ${event.wo.name}`);
});


// this is fired when the client releases object
ee.on("ReleaseObject", (event) => {
    // this can be super spammy
    //ee.log(`got ReleaseObject: ${event.wo.name} (${event.wo.id})`);
});


// this is fired when the character completely logs in
// this is not guaranteed to always fire, for example if
// the script was started after the character was already
// logged in.
ee.on("LoginComplete", (event) => {
    ee.log(`Login Complete!`);
});

// handle when ee is done registering with the host app, you script
// can now run and handle events.
ee.on("ready", () => {
    ee.log('ready freddy');

    // listen for *any* change on the character
    ee.character.on("change", (property, value) => {
        ee.log(`generic character change: ${property} changed to ${value}`);
    });

    // listen for a specific property change on the character, like health
    ee.character.on("change_health", (value) => {
        ee.log(`character health changed to ${value}`);
    });

    // start a timer to say hello in the chat window
    intervalId = setInterval(() => {
        ee.logToChatWindow("Hello from setInterval.", Math.floor(Math.random() * 32));
    }, 5000)
});

// handle when ee is told to shutdown for some reason (char log off,
// client closed, etc)
// You should clear any timers you set, and do any cleanup here.
ee.on("shutdown", () => {
    // clean up, we are shutting down
    ee.log("deady freddy");

    // stop our interval timer
    if (typeof(intervalId) !== "undefined") {
        clearInterval(intervalId);
    }
});

// *ALL* scripts must return ee.register();
return ee.register();
