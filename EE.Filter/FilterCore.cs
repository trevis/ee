﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

using Decal.Adapter;

namespace EE.Filter
{
	[FriendlyName("EE.Filter")]
	public class FilterCore : FilterBase
	{
		internal static string PluginName = "EE.Filter";

        private DateTime lastThought = DateTime.MinValue;

        // gets the direction this assembly is running from
        public static string AssemblyDirectory {
            get {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return System.IO.Path.GetDirectoryName(path);
            }
        }

        protected override void Startup() {
            EnsureEEHostIsRunning();
            CoreManager.Current.RenderFrame += Current_RenderFrame;
        }

        private void Current_RenderFrame(object sender, EventArgs e) {
            try {
                if (DateTime.UtcNow - lastThought > TimeSpan.FromSeconds(5)) {
                    lastThought = DateTime.UtcNow;

                    EnsureEEHostIsRunning();
                }
            }
            catch (Exception ex) { }
        }

        protected override void Shutdown() {
            CoreManager.Current.RenderFrame -= Current_RenderFrame;
        }

        // make sure ee.host app is running
        private void EnsureEEHostIsRunning() {
            var runningProcessByName = Process.GetProcessesByName("EE.Host");
            if (runningProcessByName.Length == 0) {
                Process.Start(AssemblyDirectory + @"\EE.Host.exe");
            }
        }
	}
}
