﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EE.Host.Config {
    [Serializable]
    public class CharacterConfig {
        int _Version;
        string _StringItem;
        int _IntItem;
        private List<string> scripts = new List<string>();

        public CharacterConfig() {
            _Version = 1;
            _StringItem = "";
            _IntItem = -1;
        }

        public static void Serialize(string file, CharacterConfig c) {
            FileInfo fileInfo = new FileInfo(file);

            if (!fileInfo.Exists)
                Directory.CreateDirectory(fileInfo.Directory.FullName);

            System.Xml.Serialization.XmlSerializer xs
               = new System.Xml.Serialization.XmlSerializer(c.GetType());
            StreamWriter writer = File.CreateText(file);
            xs.Serialize(writer, c);
            writer.Flush();
            writer.Close();
        }

        public static CharacterConfig Deserialize(string file) {
            if (!File.Exists(file)) return new CharacterConfig();

            System.Xml.Serialization.XmlSerializer xs
               = new System.Xml.Serialization.XmlSerializer(
                  typeof(CharacterConfig));
            StreamReader reader = File.OpenText(file);
            CharacterConfig c = (CharacterConfig)xs.Deserialize(reader);
            reader.Close();
            return c;
        }

        public int Version {
            get { return _Version; }
            set { _Version = value; }
        }

        public string StringItem {
            get { return _StringItem; }
            set { _StringItem = value; }
        }

        public int IntItem {
            get { return _IntItem; }
            set { _IntItem = value; }
        }

        public List<string> Scripts {
            get { return scripts; }
            set { scripts = value; }
        }

    }
}
