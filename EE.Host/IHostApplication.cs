﻿using System;
using EE.Services;

namespace EE.Host
{
    /// <summary>
    /// Interface for defining the host application.
    /// </summary>
    public interface IHostApplication
    {
        /// <summary>
        /// Invoked when a new product was added.
        /// </summary>
        event EventHandler<EventArgs> ProductAdded;
    }
}
