﻿using EE.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EE.Host {
    public static class ScriptController {
        private static ulong _sidCounter = 0;

        // client uid is they key
        private static Dictionary<string, Dictionary<string, Script>> scripts = new Dictionary<string, Dictionary<string, Script>>();

        public static string RunScriptFileOnClient(string uid, string scriptName, IDecalServiceCallback callback) {
            try {
                // TODO: read from package.json instead of using index.json
                var absolutePath = Path.GetFullPath(@"C:\Games\ExpressionEngine\scripts\" + scriptName + @"\index.js");

                if (File.Exists(absolutePath)) {
                    var scriptContents = File.ReadAllText(absolutePath);

                    return RunScriptContentsOnClient(uid, scriptName, scriptContents, callback);
                }
                else {
                    Form1.c.WriteLine(String.Format("Script not found: {0}", absolutePath));

                    callback.Error(new DecalServiceError(
                        DecalServiceErrorCode.ERROR_SCRIPT_FILE_NOT_FOUND,
                        string.Format("Script not found: {0}", absolutePath)
                    ));
                }
            }
            catch (Exception e) { Program.LogException(e); }

            return "";
        }

        public static string RunScriptContentsOnClient(string uid, string scriptName, string scriptContents, IDecalServiceCallback callback) {
            var sid = (++_sidCounter).ToString().PadLeft(4, '0');
            Script script = new Script(uid, sid, scriptName, scriptContents, callback);

            if (!scripts.ContainsKey(uid)) {
                scripts.Add(uid, new Dictionary<string, Script>());
            }

            scripts[uid].Add(sid, script);

            return sid;
        }

        public static List<string> GetAvailableScripts() {
            var availableScripts = new List<string>();
            var scriptDirectory = Utils.GetScriptsDirectory();

            foreach (var directory in Directory.GetDirectories(scriptDirectory)) {
                // TODO: read package.json and find script entry point
                if (File.Exists(directory + @"\index.js") && File.Exists(directory + @"\package.json")) {
                    availableScripts.Add(directory.Replace(scriptDirectory, ""));
                }
            }

            return availableScripts;
        }

        public static void CallClientEventHandlers(string uid, string eventName, object args) {
            try {
                if (scripts.ContainsKey(uid)) {
                    foreach (var scriptEntry in scripts[uid]) {
                        try {
                            if (scriptEntry.Value != null) {
                                scriptEntry.Value.CallEventHandlers(eventName, args);
                            }
                        }
                        catch (Exception e) { Program.LogException(e); }
                    }
                }
            }
            catch (Exception e) { Program.LogException(e); }
        }

        public static void ShutdownClient(string uid) {
            try {
                if (scripts.ContainsKey(uid)) {
                    Form1.c.WriteLine("Shutting down client: " + uid);

                    foreach (var scriptEntry in scripts[uid]) {
                        if (scriptEntry.Value != null) {
                            try {
                                scriptEntry.Value.Shutdown();
                            }
                            catch (Exception e) { Program.LogException(e); }
                        }
                    }

                    scripts.Remove(uid);
                }


            }
            catch (Exception e) { Program.LogException(e); }
        }

        public static void ShutdownScript(string uid, string sid) {
            try {
                if (scripts.ContainsKey(uid) && scripts[uid].ContainsKey(sid)) {
                    var script = scripts[uid][sid];

                    Form1.c.WriteLine(string.Format("Shutting down script: {0}.{1}", uid, sid));

                    script.Shutdown();
                    scripts[uid].Remove(sid);
                }
            }
            catch (Exception e) { Program.LogException(e); }
        }

        internal static Script GetScript(string uid, string sid) {
            if (scripts.ContainsKey(uid) && scripts[uid].ContainsKey(sid)) {
                return scripts[uid][sid];
            }

            return null;
        }

        internal static Script GetScriptByName(string uid, string scriptName) {
            if (scripts.ContainsKey(uid)) {
                foreach (var script in scripts[uid].Values) {
                    if (script.scriptName == scriptName) {
                        return script;
                    }
                }
            }

            return null;
        }
    }
}
