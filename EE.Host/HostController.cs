﻿using System;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Configuration;
using System.Threading;
using System.Windows;
using EE.Services;

namespace EE.Host
{
    /// <summary>
    /// The host/server controller
    /// </summary>
    public partial class HostController : Application, IHostApplication
    {
        #region Constructor

        /// <summary>
        /// Creates a new instance of the <see cref="HostController"/> class.
        /// </summary>
        public HostController()
        {
            Activated += HostControllerActivated;
        }

        #endregion

        #region Initialization

        public void Init()
        {
            try {
                //start a new thread to start the server
                var initializationThread = new Thread(StartHost);
                initializationThread.Start();
            }
            catch (Exception e) { Program.LogException(e); }
        }

        private void StartHost()
        {
            try {
                var group =
                    ServiceModelSectionGroup.GetSectionGroup(
                    ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None));

                if (group != null) {
                    //take the first service and endpoint definition
                    var service = group.Services.Services[0];
                    var baseAddress =
                                service.Endpoints[0].Address.AbsoluteUri.Replace(service.Endpoints[0].Address.AbsolutePath,
                                                                                    String.Empty);
                    //create service
                    var nodeService = typeof(NodeService);
                    //create host
                    var host = new ServiceHost(nodeService, new[] { new Uri(baseAddress) });
                    host.AddServiceEndpoint(typeof(IDecalService),
                                            new NetNamedPipeBinding(), service.Endpoints[0].Address.AbsolutePath);

                    try {
                        //open host
                        host.Open();
                    }
                    catch (Exception e) { Program.LogException(e); }
                }
            }
            catch (Exception e) { Program.LogException(e); }
        }

        #endregion

        #region Event Handler

        public void OnProductAdded(string productNumber)
        {
            try {
                ProductAdded?.Invoke(this, new EventArgs());
            }
            catch (Exception e) { Program.LogException(e); }
        }

        void HostControllerActivated(object sender, EventArgs e)
        {
            try {
                Init();
                MainWindow.DataContext = this;
                Activated -= HostControllerActivated;
            }
            catch (Exception ex) { Program.LogException(ex); }
        }

        #endregion

        #region IHostApplication members

        /// <summary>
        /// Invoked when a new product was added.
        /// </summary>
        public event EventHandler<EventArgs> ProductAdded;

        #endregion
        
    }
}
