﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EE.Host {
    class Utils {
        public static string GetConfigDirectory() {
            return @"C:\Games\ExpressionEngine\config\";
        }

        public static string GetScriptsDirectory() {
            return @"C:\Games\ExpressionEngine\scripts\";
        }
    }
}
