﻿using EdgeJs;
using EE.Services;
using EE.Services.EventArgs;
using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Dynamic;
using EE.Host.Config;

namespace EE.Host {
    /// <summary>
    /// Product service implementation.
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)]
    public class NodeService : IDecalService {
        private static int _uidCounter = 0;

        public delegate void Add(string uid);
        
        private string uid = "0";
        private string server = "";
        private string character = "";
        private IDecalServiceCallback callback;

        private Dictionary<string, Thread> scriptTasks = new Dictionary<string, Thread>();
        private bool isRunning = true;
        private readonly System.Timers.Timer checkConnectionTimer;
        private DateTime lastPingResponse = DateTime.UtcNow;

        const double CLIENT_TIMEOUT = 5;

        /// <summary>
        /// Creates a new instance of the <see cref="NodeService"/> class.
        /// </summary>
        public NodeService() {
            try {
                this.uid = (++NodeService._uidCounter).ToString().PadLeft(4, '0');

                Program.mainForm.button1.Click += ConsoleButtonClicked;

                Form1.c.WriteLine("uid " + uid + " connected");

                this.checkConnectionTimer = new System.Timers.Timer(1000);
                this.checkConnectionTimer.Elapsed += HeartbeatTimer;

                this.checkConnectionTimer.Start();

                Form1.tree.Invoke((MethodInvoker)delegate () {
                    var characterNode = Form1.characterTree.Nodes.Find(uid, false);
                    if (characterNode.Length == 0) {
                        Form1.characterTree.Nodes.Add(uid, String.Format("{0}", uid));
                    }
                    Form1.characterTree.ExpandAll();
                });
            }
            catch (Exception e) { Program.LogException(e); }
        }

        void Shutdown() {
            try {
                isRunning = false;

                this.checkConnectionTimer.Elapsed -= HeartbeatTimer;
                this.checkConnectionTimer.Stop();
                this.checkConnectionTimer.Dispose();

                ScriptController.ShutdownClient(uid);

                Form1.tree.Invoke((MethodInvoker)delegate () {
                    Form1.characterTree.Nodes.RemoveByKey(uid);
                });

                Form1.c.WriteLine("uid " + uid + " is shut down");

                Program.mainForm.button1.Click -= ConsoleButtonClicked;

            }
            catch (Exception e) { Program.LogException(e); }
        }

        void HeartbeatTimer(object sender, System.Timers.ElapsedEventArgs e) {
            try {
                if (!isRunning) return;

                if (DateTime.UtcNow - lastPingResponse > TimeSpan.FromSeconds(CLIENT_TIMEOUT)) {
                    Form1.c.WriteLine("Shutting down " + uid + " because no heartbeat response in " + CLIENT_TIMEOUT + " seconds.");
                    Shutdown();
                    return;
                }

                if (callback != null) {
                    try {
                        callback.Ping();
                    }
                    catch (Exception ex) { }
                }
            }
            catch (Exception ex) { Program.LogException(ex); }
        }

        private void ConsoleButtonClicked(object sender, EventArgs e) {
            try {
                if (callback != null) {
                    callback.AddChatText("Console button was clicked AddChatText", 3);
                }
            }
            catch (Exception ex) { Program.LogException(ex); }
        }

        private void CallEventHandlers(string eventName, object args) {
            try {
                ScriptController.CallClientEventHandlers(uid, eventName, args);
            }
            catch (Exception e) { Form1.LogException(e); Program.LogException(e); }
        }

        #region Implementation of IDecalService

        /// <summary>
        /// Register a client with host app
        /// </summary>
        public void RegisterClient(string serverName, string characterName)
        {
            this.server = serverName;
            this.character = characterName;

            Form1.tree.Invoke((MethodInvoker)delegate () {
                var characterNode = Form1.characterTree.Nodes.Find(uid, false);
                characterNode[0].Text = String.Format("{0}/{1}", server, character);

                Form1.characterTree.ExpandAll();
            });

            Form1.c.WriteLine(String.Format("[{0}] registered as: {1}/{2}", uid, server, character));

            //get callback from operation context
            var callback = OperationContext.Current.GetCallbackChannel<IDecalServiceCallback>();

            if (callback != null) {
                this.callback = callback;

                try {
                    callback.Registered(uid);

                    LoadCharacterScripts();
                }
                catch (Exception e) { Program.LogException(e); }
            }
        }

        private CharacterConfig GetCharacterConfig() {
            var configFile = Utils.GetConfigDirectory() + server + @"\" + character + @"\config.xml";
            CharacterConfig config = CharacterConfig.Deserialize(configFile);

            return config;
        }

        private void SaveCharacterConfig(CharacterConfig config) {
            var configFile = Utils.GetConfigDirectory() + server + @"\" + character + @"\config.xml";

            CharacterConfig.Serialize(configFile, config);
        }

        private void LoadCharacterScripts() {
            try {
                var config = GetCharacterConfig();

                foreach (var script in config.Scripts) {
                    try {
                        RunClientScript(script);
                    }
                    catch (Exception e) { Program.LogException(e); }
                }
            }
            catch (Exception e) { Program.LogException(e); }
        }

        public void UnregisterClient() {
            try {
                ScriptController.ShutdownClient(uid);
                Shutdown();
            }
            catch (Exception e) { Program.LogException(e); }
        }

        public void RequestAvailableScripts() {
            try {
                var callback = OperationContext.Current.GetCallbackChannel<IDecalServiceCallback>();

                if (callback != null) {
                    callback.AvailableScripts(ScriptController.GetAvailableScripts());
                }
            }
            catch (Exception e) { Program.LogException(e); }
        }

        public void Pong() {
            try {
                var callback = OperationContext.Current.GetCallbackChannel<IDecalServiceCallback>();

                if (callback != null) {
                    this.callback = callback;
                }

                lastPingResponse = DateTime.UtcNow;
            }
            catch (Exception e) { Program.LogException(e); }
        }

        public void RunClientScript(string scriptName) {
            try {
                var callback = OperationContext.Current.GetCallbackChannel<IDecalServiceCallback>();
                var config = GetCharacterConfig();

                if (!config.Scripts.Contains(scriptName)) {
                    config.Scripts.Add(scriptName);
                    SaveCharacterConfig(config);
                }

                var runningScript = ScriptController.GetScriptByName(uid, scriptName);

                if (runningScript != null) {
                    if (callback != null) {
                        callback.Error(
                            new DecalServiceError(DecalServiceErrorCode.ERROR_SCRIPT_ALREADY_RUNNING,
                            string.Format("Script {0} is already running!", scriptName))
                        );
                    }
                    return;
                }

                try {
                    var sid = ScriptController.RunScriptFileOnClient(uid, scriptName, callback);

                    if (!string.IsNullOrEmpty(sid)) {
                        Form1.tree.Invoke((MethodInvoker)delegate () {
                            Form1.characterTree.Nodes[uid].Nodes.Add(sid, String.Format("{0}.{1}", scriptName, sid));
                            Form1.tree.ExpandAll();
                        });
                    }
                }
                catch (Exception e) { Program.LogException(e); }
            }
            catch (Exception ex) { Program.LogException(ex); }
        }

        public void StopClientScript(string sid) {
            try {
                ScriptController.ShutdownScript(uid, sid);
                var config = GetCharacterConfig();
                var script = ScriptController.GetScript(uid, sid);

                if (script != null && config.Scripts.Contains(script.scriptName)) {
                    config.Scripts.Remove(script.scriptName);
                    SaveCharacterConfig(config);
                }

                if (Form1.characterTree.Nodes.ContainsKey(uid) && Form1.characterTree.Nodes[uid].Nodes.ContainsKey(sid)) {
                    Form1.tree.Invoke((MethodInvoker)delegate () {
                        Form1.characterTree.Nodes[uid].Nodes[sid].Remove();
                        Form1.tree.ExpandAll();
                    });
                }
            }
            catch (Exception e) { Program.LogException(e); }
        }

        public void OnChangeFellowship(ChangeFellowshipEventArgsWrapper e) {
            try {
                CallEventHandlers("ChangeFellowship", e);
            }
            catch (Exception ex) { Program.LogException(ex); }
        }

        public void OnChatBoxMessage(ChatTextInterceptEventArgsWrapper e) {
            try {
                CallEventHandlers("ChatBoxMessage", e);
            }
            catch (Exception ex) { Program.LogException(ex); }
        }

        public void OnChatNameClicked(ChatClickInterceptEventArgsWrapper e) {
            try {
                CallEventHandlers("ChatNameClicked", e);
            }
            catch (Exception ex) { Program.LogException(ex); }
        }

        public void OnCommandLineText(ChatParserInterceptEventArgsWrapper e) {
            try {
                CallEventHandlers("CommandLineText", e);
            }
            catch (Exception ex) { Program.LogException(ex); }
        }

        public void OnContainerOpened(ContainerOpenedEventArgsWrapper e) {
            try {
                CallEventHandlers("ContainerOpened", e);
            }
            catch (Exception ex) { Program.LogException(ex); }
        }

        public void OnCreateObject(CreateObjectEventArgsWrapper e) {
            try {
                var d = EEWorldObjectToDynamic(e.wo);
                CallEventHandlers("_CreateObject", new {
                    wo = d
                });
            }
            catch (Exception ex) { Program.LogException(ex); }
        }

        public void OnChangeObject(ChangeObjectEventArgsWrapper e) {
            try {
                var d = EEWorldObjectToDynamic(e.wo);
                CallEventHandlers("_ChangeObject", new {
                    wo = d,
                    type = e.type
                });
            }
            catch (Exception ex) { Program.LogException(ex); }
        }

        public void OnReleaseObject(ReleaseObjectEventArgsWrapper e) {
            try {
                var d = EEWorldObjectToDynamic(e.wo);
                CallEventHandlers("_ReleaseObject", new {
                    wo = d
                });
            }
            catch (Exception ex) { Program.LogException(ex); }
        }

        public void OnChangeVital(ChangeVitalEventArgsWrapper e) {
            try {
                CallEventHandlers("ChangeVital", e);
            }
            catch (Exception ex) { Program.LogException(ex); }
        }

        public void OnLoginComplete() {
            try {
                CallEventHandlers("LoginComplete", null);
            }
            catch (Exception ex) { Program.LogException(ex); }
        }

        public void OnChangeCharacter(EECharacter eec) {
            try {
                CallEventHandlers("_ChangeCharacter", new {
                    character = EECharacterToDynamic(eec)
                });
            }
            catch (Exception ex) { Program.LogException(ex); }
        }

        private dynamic EECharacterToDynamic(EECharacter wo) {
            dynamic obj = new ExpandoObject();
            var dObj = (IDictionary<string, object>)obj;

            // copy over base properties
            foreach (var prop in wo.GetType().GetProperties()) {
                dObj.Add(prop.Name, (object)prop.GetValue(wo, null));
            }

            return obj;
        }

        private dynamic EEWorldObjectToDynamic(EEWorldObject wo) {
            dynamic obj = new ExpandoObject();
            var dObj = (IDictionary<string, object>)obj;

            // copy over base wo data
            foreach (var prop in wo.GetType().GetProperties()) {
                dObj.Add(prop.Name, (object)prop.GetValue(wo, null));
            }

            // value keys
            obj.boolValues = ValuesToDynamic(wo.boolValues);
            obj.stringValues = ValuesToDynamic(wo.stringValues);
            obj.doubleValues = ValuesToDynamic(wo.doubleValues);
            obj.longValues = ValuesToDynamic(wo.longValues);

            return obj;
        }

        private dynamic ValuesToDynamic(dynamic values) {
            dynamic obj = new ExpandoObject();
            var dValues = (IDictionary<string, object>)obj;

            foreach (var entry in values) {
                dValues.Add(((int)entry.Key).ToString(), (object)entry.Value);
            }

            return obj;
        }

        #endregion
    }
}
