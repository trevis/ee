﻿using EdgeJs;
using EE.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EE.Host {
    class Script {
        public Thread thread;

        private IDecalServiceCallback callback;
        public string uid;
        public string sid;
        public string scriptName;
        private string scriptModulesPath;
        private string scriptContents;
        private CancellationTokenSource cancellationTokenSource;

        private Dictionary<string, List<Func<object, Task<object>>>> eventHandlers = new Dictionary<string, List<Func<object, Task<object>>>>();

        public Script(string _uid, string _sid, string _scriptName, string _scriptContents, IDecalServiceCallback _callback) {
            uid = _uid;
            sid = _sid;
            scriptName = _scriptName;
            scriptContents = _scriptContents;
            callback = _callback;

            scriptModulesPath = @"C:\\Games\\ExpressionEngine\\Scripts\\" + scriptName + @"\\node_modules";

            cancellationTokenSource = new CancellationTokenSource();

            Task.Factory.StartNew(async () => {
                await Start(cancellationTokenSource.Token);
            });
        }

        public void AddNewEventHandler(string eventName, Func<object, Task<object>> func) {
            try {
                if (eventHandlers.ContainsKey(eventName)) {
                    eventHandlers[eventName].Add(func);
                }
                else {
                    eventHandlers.Add(eventName, new List<Func<object, Task<object>>>());
                    eventHandlers[eventName].Add(func);
                }
            }
            catch (Exception e) { Program.LogException(e); }
        }

        public void CallEventHandlers(string eventName, object args) {
            try {
                if (eventHandlers.ContainsKey(eventName)) {
                    foreach (var eventHandler in eventHandlers[eventName]) {
                        eventHandler(args);
                    }
                }
            }
            catch (Exception e) { Form1.LogException(e);  Program.LogException(e); }
        }

        public async Task Start(CancellationToken ct) {
            this.thread = Thread.CurrentThread;

            /*
             * tell script host that the script is done with init stuff
             */
            var ready = (Func<object, Task<object>>)(async (dynamic input) => {
                try {
                    try {
                        callback.NewScriptRunning(sid, scriptName);
                    }
                    catch (Exception e) { }

                    // notify our scripts we are ready
                    CallEventHandlers("ready", new {
                        uid,
                        sid
                    });
                }
                catch (Exception e) { Form1.LogException(e); Program.LogException(e); }

                return true;
            });

            /*
             * register callback for event 
             */
            var registerForEvent = (Func<object, Task<object>>)(async (dynamic input) => {
                try {
                    AddNewEventHandler((string)input.eventName, (Func<object, Task<object>>)input.callback);
                }
                catch (Exception e) { Form1.LogException(e); Program.LogException(e); }

                return true;
            });

            /*
             * adds text to the clients chatbox, as if they typed it in
             */
            var invokeChatParser = (Func<object, Task<object>>)(async (command) => {
                try {
                    if (callback != null) {
                        callback.InvokeChatParser((string)command);
                    }
                }
                catch (Exception e) { Form1.LogException(e); Program.LogException(e); }

                return true;
            });

            /*
             * echo text to the chat window, in a specific color
             */
            var logToChatWindow = (Func<object, Task<object>>)(async (dynamic input) => {
                try {
                    if (callback != null) {
                        callback.AddChatText((string)input.text, (int)input.color);
                    }
                }
                catch (Exception e) { Form1.LogException(e); Program.LogException(e); }

                return true;
            });

            // TODO: timeout when starting script?

            try {
                // not a huge fan of this but can't think of a better way...
                // this is how we add node_modules for this script to the search path,
                // we should remove it after init so it doesn't interfere with other scripts
                var bootstrappedScriptContents = @"require('./app-module-path-node/lib/index').addPath(require('path').resolve('" + scriptModulesPath + "'));";

                scriptContents = bootstrappedScriptContents + scriptContents;

                var func = Edge.Func(scriptContents);

                var jsFunc = (Func<object, Task<object>>)await func(new {
                    registerForEvent,
                    invokeChatParser,
                    logToChatWindow,
                    ready,
                    options = new {
                        uid,
                        sid,
                        scriptName,
                        scriptModulesPath = scriptModulesPath.Replace(@"\\", @"\")
                    }
                });
            }
            catch (Exception e) { Form1.LogException(e); Program.LogException(e); }
        }

        public void Shutdown() {
            try {
                CallEventHandlers("shutdown", (object)null);

                try {
                    callback.ScriptStopped(sid, scriptName);
                }
                catch (Exception e) { }
            }
            catch (Exception e) { Program.LogException(e); }
            
            cancellationTokenSource.Cancel();

            // TODO: better shutdown
            System.Threading.Timer timer = null;
            timer = new System.Threading.Timer((obj) =>
            {
                thread.Abort();
                timer.Dispose();
                cancellationTokenSource.Dispose();
            },
                        null, 10000, System.Threading.Timeout.Infinite);
        }
    }
}
