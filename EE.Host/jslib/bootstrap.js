﻿const path = require('path');

// add jslib to our module search paths
const pathHelper = require('./app-module-path-node/lib/index')
pathHelper.addPath(path.resolve(__dirname + "\\..\\jslib"));

// this is our eehost instance, it provides a global interface to all running scripts/characters
var eehost = require('ee/host');

eehost.removeRequirePath = (path) => {
    pathHelper.removePath(path);
}

// expose ee host globally
global.eehost = eehost;

return eehost.register();