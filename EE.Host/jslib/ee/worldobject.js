﻿// private members symbols
const applyProperty = Symbol("applyProperty");
const applyValueKeys = Symbol("applyValueKeys");

class WorldObject {
    constructor(wo) {
        this.boolValues = {};
        this.longValues = {};
        this.stringValues = {};
        this.doubleValues = {};

        if (typeof wo === "number") {
            this.id = wo;
        }
        else {
            this.id = wo.id;
            this.updateValuesFromRawWorldObject(wo);
        }
    }

    [applyProperty](property, value) {
        if (this[property] !== value) {
            this[property] = value;
            return true;
        }

        return false;
    }

    [applyValueKeys](property, value) {
        var hasChanges = false;

        if (typeof this[property] !== "object") {
            this[property] = {};
        }

        Object.keys(value).forEach((key) => {
            if (this[property][key] !== value[key]) {
                this[property][key] = value[key];
                hasChanges = true;
            }
        });

        return hasChanges;
    }

    updateValuesFromRawWorldObject(wo) {
        const keys = Object.keys(wo);
        let hasChanges = false;

        keys.forEach((key) => {
            switch (typeof wo[key]) {
                case 'number':
                case 'string':
                case 'boolean':
                    if (this[applyProperty](key, wo[key])) {
                        hasChanges = true;
                    }
                    break;

                case 'object':
                    if (this[applyValueKeys](key, wo[key])) {
                        hasChanges = true;
                    }
                    break;
            }
        });

        return hasChanges;
    }
}

module.exports = WorldObject;