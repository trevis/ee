﻿const EventEmitter = require('events');

const eehost = require('./host');
const WorldObject = require('./worldobject');
const Character = require('./character');

// private members
const checkEventRegistration = Symbol("checkEventRegistration");
const handleChangeObject = Symbol("handleChangeObject");
const handleCreateObject = Symbol("handleCreateObject");
const handleReleaseObject = Symbol("handleReleaseObject");
const createWorldObject = Symbol("createWorldObject");
const handleChangeCharacter = Symbol("handlerChangeCharacter");
const handleShutdown = Symbol("handleShutdown");

class ExpressionEngineScript {
    constructor() {
        // wrapped event emiiter
        this._eventEmitter = new EventEmitter();

        // host interface registration state
        this._isRegistered = false;

        this.sid = "";
        this.uid = "";

        // this is our character
        this.character = new Character(0);

        // this is the interface that our dotnet code exposes to us.
        // we can do things like log or register for events with it.
        // note: it is only available after registering
        this._interface = undefined;

        // all world objects know to this script
        this._worldObjects = {};

        // internal
        this.on("_ChangeObject", this[handleChangeObject].bind(this));
        this.on("_CreateObject", this[handleCreateObject].bind(this));
        this.on("_ReleaseObject", this[handleReleaseObject].bind(this));
        this.on("_ChangeCharacter", this[handleChangeCharacter].bind(this));
        this.on("Shutdown", this[handleShutdown].bind(this));
        this._eventEmitter.on("error", (error) => {
            ee.log(error.stack);
        })
    }

    // check if we are registered with the host interface
    // returns true if we are registered for events with the host interface
    isRegistered() {
        return this._isRegistered;
    }

    // register for a decal / host event
    on(eventName, callback) {
        if (this.isRegistered()) {
            this[checkEventRegistration](eventName);
        }

        this._eventEmitter.on(eventName, callback);
    }

    [checkEventRegistration](eventName) {
        var that = this;

        // register with the host to listen to this event if needed
        if (this._eventEmitter.eventNames().indexOf(eventName) === -1) {
            this._interface.registerForEvent({
                eventName: eventName,
                callback: (data, cb) => {
                    // emit this event on our ee object
                    try {
                        that._eventEmitter.emit(eventName, data);
                    }
                    catch (err) { ee.log(err.stack); }

                    cb();
                }
            });
        }
    }

    [handleShutdown]() {
        this._eventEmitter.eventNames().forEach((eventName) => {
            this._eventEmitter.removeAllListeners(eventName);
        });

        this._worldObjects = {};
        this._isRegistered = false;
        this._interface = undefined;
    }

    [handleChangeCharacter](event) {
        this.character.updateValuesFromRawCharacter(event.character);
    }

    [handleChangeObject](event) {
        var wo = this.getWorldObject(event.wo.id);

        if (wo) {
            wo.updateValuesFromRawWorldObject(event.wo);
        }
        else {
            wo = this[createWorldObject](event.wo);
        }

        this._eventEmitter.emit("ChangeObject", {
            type: event.type,
            wo: wo
        });
    }

    [handleCreateObject](event) {
        var wo = this.getWorldObject(event.wo.id);

        if (wo) {
            wo.updateValuesFromRawWorldObject(event.wo);
        }
        else {
            wo = this[createWorldObject](event.wo);
        }

        this._eventEmitter.emit("CreateObject", {
            wo: wo
        });
    }

    [handleReleaseObject](event) {
        var wo = this.getWorldObject(event.wo.id);

        if (wo) {
            setTimeout(() => {
                delete this._worldObjects[wo.id];
            }, 10);
        }

        this._eventEmitter.emit("ReleaseObject", {
            wo: wo
        });
    }

    [createWorldObject](wo) {
        this._worldObjects[wo.id] = new WorldObject(wo);

        return this.getWorldObject(wo.id);
    }

    // gets a world object by id, from the cache
    getWorldObject(id) {
        return this._worldObjects[id];
    }

    // log a message to the host interface console
    // note: only available after registering
    log(message) {
        eehost.log(`[${this.uid}.${this.sid}] ${message}`);
    }

    //logToChatWindow
    logToChatWindow(text, color) {
        if (this._interface) {
            this._interface.logToChatWindow({ text, color });
        }
    }

    // add a command to the client's chat box
    invokeChatParser(command) {
        if (this._interface) {
            this._interface.invokeChatParser(command);
        }
    }

    // ee scripts must return the results of this function in the main script
    // this will register with the interfact for any wanted events
    // eg: `myScriptInit(); return ee.register(options);`
    register(options) {
        var that = this;

        return (_interface, callback) => {
            that._interface = _interface;

            that.sid = _interface.options.sid;
            that.uid = _interface.options.uid;

            // remove ourselves from global node modules paths, dirty dirty
            eehost.removeRequirePath(_interface.options.scriptModulesPath);


            that.log(`ExpressionEngineScript init [${that.uid}.${that.sid}]`);

            // register for any events that have been subscribed to
            that._eventEmitter.eventNames().forEach((eventName) => {
                // registers through the interface to be notified on a specific event
                _interface.registerForEvent({
                    eventName: eventName,
                    callback: (data, cb) => {
                        // emit this event on our ee object
                        try {
                            that._eventEmitter.emit(eventName, data);
                        }
                        catch (err) { that.log(err.stack); }
                        cb();
                    }
                });
            });


            // flag ourselves as registered (for events)
            that._isRegistered = true;

            that._interface.ready();

            callback();
        }
    }
}

module.exports = ExpressionEngineScript;