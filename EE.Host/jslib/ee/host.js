﻿const EventEmitter = require('events');

const isRegistered = Symbol("isRegistered");
const hostInterface = Symbol("interface");

class ExpressionEngineHost extends EventEmitter {
    constructor() {
        super();

        this[isRegistered] = false;
        this[hostInterface] = null;

        console.log("EEHost init");
    }

    log(message) {
        if (this[hostInterface] && this[hostInterface].log) {
            this[hostInterface].log(message);
        }
    }

    register(options) {
        var that = this;

        if (this[isRegistered]) {
            this.log("EEHost.register called more than once!");
            return;
        }

        return (_interface, callback) => {
            that[hostInterface] = _interface;

            that.log("ExpressionEngineHost init");

            // flag ourselves as registered (for events)
            that[isRegistered] = true;

            //that[interface].ready();

            callback();
        }
    }
}

const eehost = new ExpressionEngineHost();

module.exports = eehost;