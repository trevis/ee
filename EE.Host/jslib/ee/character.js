﻿const EventEmitter = require('events');

// private members symbols
const applyProperty = Symbol("applyProperty");
const applyValueKeys = Symbol("applyValueKeys");

class Character extends EventEmitter {
    constructor(cf) {
        super();

        if (typeof cf === "number") {
            this.id = cf;
        }
        else {
            this.id = cf.id;
            this.updateValuesFromRawCharacter(cf);
        }
    }

    [applyProperty](property, value) {
        if (this[property] !== value) {
            this[property] = value;
            this.emit(`change_${property}`, value);
            this.emit('change', property, value);
            return true;
        }

        return false;
    }

    updateValuesFromRawCharacter(cf) {
        const keys = Object.keys(cf);
        let hasChanges = false;

        keys.forEach((key) => {
            switch (typeof cf[key]) {
                case 'number':
                case 'string':
                case 'boolean':
                    if (this[applyProperty](key.charAt(0).toLowerCase() + key.slice(1), cf[key])) {
                        hasChanges = true;
                    }
                    break;
            }
        });

        return hasChanges;
    }
}

module.exports = Character;