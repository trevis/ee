﻿const CharFilterVitalType = Object.freeze({
    Health: 2,
    Stamina: 4,
    Mana: 6
});

module.exports = CharFilterVitalType;