﻿const FellowshipEventType = Object.freeze({
    Create: 0,
    Quit: 1,
    Dismiss: 2,
    Recruit: 3,
    Disband: 4,
});

module.exports = FellowshipEventType;