﻿const WorldChangeType = Object.freeze({
    SizeChange: 0,
    StorageChange: 1,
    IdentReceived: 2,
    VendorIdentReceived: 3,
    ManaChange: 4
});

module.exports = WorldChangeType;