﻿namespace EE.Host {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.fConsole1 = new WindowsForm.Console.FConsole();
            this.button1 = new System.Windows.Forms.Button();
            this.instanceTree = new System.Windows.Forms.TreeView();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // fConsole1
            // 
            this.fConsole1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fConsole1.Arguments = new string[0];
            this.fConsole1.AutoScrollToEndLine = true;
            this.fConsole1.BackColor = System.Drawing.Color.Black;
            this.fConsole1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.fConsole1.Font = new System.Drawing.Font("Consolas", 10F);
            this.fConsole1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(216)))), ((int)(((byte)(194)))));
            this.fConsole1.HyperlinkColor = System.Drawing.Color.Empty;
            this.fConsole1.Location = new System.Drawing.Point(187, 38);
            this.fConsole1.MinimumSize = new System.Drawing.Size(470, 200);
            this.fConsole1.Name = "fConsole1";
            this.fConsole1.ReadOnly = true;
            this.fConsole1.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.fConsole1.SecureReadLine = true;
            this.fConsole1.Size = new System.Drawing.Size(535, 398);
            this.fConsole1.State = WindowsForm.Console.Enums.ConsoleState.Writing;
            this.fConsole1.TabIndex = 0;
            this.fConsole1.Text = "";
            this.fConsole1.Title = "EE.Host";
            this.fConsole1.TextChanged += new System.EventHandler(this.fConsole1_TextChanged);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(640, 9);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(82, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Test Button";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // instanceTree
            // 
            this.instanceTree.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.instanceTree.Location = new System.Drawing.Point(12, 38);
            this.instanceTree.Name = "instanceTree";
            this.instanceTree.Size = new System.Drawing.Size(169, 398);
            this.instanceTree.TabIndex = 2;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 439);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(734, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(249, 17);
            this.toolStripStatusLabel1.Text = "Right click on an item above for more options";
            this.toolStripStatusLabel1.Click += new System.EventHandler(this.toolStripStatusLabel1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 461);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.instanceTree);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.fConsole1);
            this.MinimumSize = new System.Drawing.Size(750, 500);
            this.Name = "Form1";
            this.Text = "EE.Host";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public WindowsForm.Console.FConsole fConsole1;
        public System.Windows.Forms.Button button1;
        private System.Windows.Forms.TreeView instanceTree;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
    }
}