﻿using EdgeJs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsForm.Console;

namespace EE.Host {
    public partial class Form1 : Form {
        public static FConsole c;
        public static TreeView tree;
        public static HostController host;
        public static TreeNode characterTree;

        public static void LogException(Exception ex) {
            if (Form1.c == null) return;

            Form1.c.WriteLine("Error: " + ex.Message);
            Form1.c.WriteLine("Source: " + ex.Source);
            Form1.c.WriteLine("Stack: " + ex.StackTrace);
            if (ex.InnerException != null) {
                Form1.c.WriteLine("Inner: " + ex.InnerException.Message);
                Form1.c.WriteLine("Inner Stack: " + ex.InnerException.StackTrace);
            }

            Program.LogException(ex);
        }

        public static string AssemblyDirectory {
            get {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        public Form1() {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
        }

        private void fConsole1_TextChanged(object sender, EventArgs e) {

        }

        private void Form1_Load(object sender, EventArgs e) {
            try {
                c = fConsole1;
                tree = instanceTree;
                InitializeTreeView();

                // enable nodejs inspector, can view with chrome://inspect
                Environment.SetEnvironmentVariable("EDGE_NODE_PARAMS", "--inspect");

                try {
                    // prime the js engine
                    var path = (AssemblyDirectory + @"\jslib\").Replace(@"\", @"\\");
                    var func = Edge.Func(File.ReadAllText(AssemblyDirectory + @"\jslib\bootstrap.js"));


                    /*
                     * Log a message to in game chat and to the host application console window
                     */
                    var log = (Func<object, Task<object>>)(async (message) => {
                        try {
                            Form1.c.WriteLine(string.Format("{0}", message));
                        }
                        catch (Exception ex) { Form1.LogException(ex); Program.LogException(ex); }

                        return true;
                    });

                    func(new {
                        log,
                    });
                }
                catch (Exception ex) { Form1.LogException(ex); Program.LogException(ex); }

                host = new HostController();
                host.Init();

                c.WriteLine("Loaded");
            }
            catch (Exception ex) { Program.LogException(ex); }
}

        private void InitializeTreeView() {
            try {
                tree.BeginUpdate();
                tree.Nodes.Clear();

                var gnode = tree.Nodes.Add("Global Scripts");
                characterTree = tree.Nodes.Add("Character Scripts");

                tree.ExpandAll();
                tree.EndUpdate();
            }
            catch (Exception e) { Program.LogException(e); }
        }

        private void button1_Click(object sender, EventArgs e) {
            try {
                host.OnProductAdded("1245");
            }
            catch (Exception ex) { Program.LogException(ex); }
        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e) {

        }
    }
}
