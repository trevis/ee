﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EE.DecalClient {
    class Script {
        private string sid;
        private string scriptName;
        private DateTime startedRunningAt;

        public Script(string sid, string scriptName) {
            this.sid = sid;
            this.scriptName = scriptName;
            this.startedRunningAt = DateTime.UtcNow;
        }

        public string GetName() {
            return string.Format("{0}.{1}", scriptName, sid);
        }

        public TimeSpan GetRuntime() {
            if (startedRunningAt != null) {
                return DateTime.UtcNow - startedRunningAt;
            }

            return TimeSpan.MinValue;
        }

        internal string GetFriendlyRuntime() {
            return Util.GetFriendlyTimeDifference(GetRuntime());
        }

        internal bool IsAlive() {
            return true;
        }
    }
}
