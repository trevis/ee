﻿using System;
using System.IO;

namespace EE.DecalClient {
	public static class Util
	{
		public static void LogException(Exception ex)
		{
			try
			{
				using (StreamWriter writer = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.Personal) + @"\Asheron's Call\" + Globals.PluginName + " errors.txt", true))
				{
					writer.WriteLine("============================================================================");
					writer.WriteLine(DateTime.Now.ToString());
					writer.WriteLine("Error: " + ex.Message);
					writer.WriteLine("Source: " + ex.Source);
					writer.WriteLine("Stack: " + ex.StackTrace);
					if (ex.InnerException != null)
					{
						writer.WriteLine("Inner: " + ex.InnerException.Message);
						writer.WriteLine("Inner Stack: " + ex.InnerException.StackTrace);
					}
					writer.WriteLine("============================================================================");
					writer.WriteLine("");
					writer.Close();
				}
			}
			catch
			{
			}
        }

        public static void WriteToChat(string message) {
            try {
                Globals.Host.Actions.AddChatText("[" + Globals.PluginName + "] " + message, 11);
            }
            catch (Exception ex) { LogException(ex); }
        }

        /*
         * returns a friendly time difference string from a TimeSpan object
         * eg: 0d3h2m1s
         */
        public static string GetFriendlyTimeDifference(TimeSpan difference) {
            string output = "";

            if (difference.Days > 0) output += difference.Days.ToString() + "d";
            if (difference.Hours > 0) output += difference.Hours.ToString() + "h";
            if (difference.Minutes > 0) output += difference.Minutes.ToString() + "m";

            output += difference.Seconds.ToString() + "s";

            return output;
        }
    }
}
