﻿using Decal.Adapter.Wrappers;
using System;
using System.IO;

using VirindiViewService.Controls;

namespace EE.DecalClient.UI.Pages {
    class ScriptsPage : IDisposable {
        VirindiViewService.ControlGroup ic = new VirindiViewService.ControlGroup();
        HudCombo UILoadScriptDropdown { get; set; }
        HudButton UIRefreshScripts { get; set; }
        HudButton UILoadScript { get; set; }

        HudList UIScriptsList { get; set; }

        MainView _mainView;

        public ScriptsPage(MainView mainView) {
            try {
                _mainView = mainView;

                UILoadScriptDropdown = mainView.view != null ? (HudCombo)mainView.view["UILoadScriptDropdown"] : new HudCombo(ic);
                UIRefreshScripts = mainView.view != null ? (HudButton)mainView.view["UIRefreshScripts"] : new HudButton();
                UILoadScript = mainView.view != null ? (HudButton)mainView.view["UILoadScript"] : new HudButton();

                UIScriptsList = mainView.view != null ? (HudList)mainView.view["UIScriptsList"] : new HudList();

                Globals.Client.ScriptStartedEvent += Client_ScriptStartedEvent;
                Globals.Client.ScriptStoppedEvent += Client_ScriptStoppedEvent;

                /*
                ScriptManager.ScriptsChangedEvent += new ScriptManager.EventDelegate(() => {
                    RefreshActiveScripts();
                });
                */

                UIRefreshScripts.Hit += (s, e) => {
                    try {
                        RefreshScriptsDropdown();
                        RefreshActiveScripts();
                    }
                    catch (Exception ex) { Util.LogException(ex); }
                };

                UILoadScript.Hit += (s, e) => {
                    try {
                        string scriptName = ((HudStaticText)UILoadScriptDropdown[UILoadScriptDropdown.Current]).Text;
                        Util.WriteToChat("Telling host to start script: " + scriptName);

                        Globals.Client.RunScript(scriptName);
                    }
                    catch (Exception ex) { Util.LogException(ex); }
                };

                UIScriptsList.Click += new HudList.delClickedControl(UIScriptsList_Click);

                RefreshScriptsDropdown();
                RefreshActiveScripts();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void Client_ScriptStartedEvent(object sender, ScriptStatusEventArgs e) {
            try {
                RefreshActiveScripts();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void Client_ScriptStoppedEvent(object sender, ScriptStatusEventArgs e) {
            try {
                RefreshActiveScripts();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void UIScriptsList_Click(object sender, int row, int col) {
            HudList.HudListRowAccessor _row = (HudList.HudListRowAccessor)UIScriptsList[row];
            string scriptID = ((HudStaticText)_row[4]).Text;
            
            if (col == 3) {
                Globals.Client.StopScript(scriptID);
            }
        }

        private void RefreshScriptsDropdown() {
            try {
                if (Globals.Client.pipeProxy != null) {
                    Globals.Client.pipeProxy.RequestAvailableScripts();
                }

                System.Threading.Timer timer = null;
                timer = new System.Threading.Timer((obj) => {
                    try {
                        var files = Globals.Client.availableScripts;

                        UILoadScriptDropdown.Clear();

                        foreach (var file in files) {
                            UILoadScriptDropdown.AddItem(file, null);
                        }
                    }
                    catch (Exception e) { Util.LogException(e); }

                    timer.Dispose();
                },
                            null, 300, System.Threading.Timeout.Infinite);
            }
            catch (Exception e) { Util.LogException(e); }
        }

        private void RefreshActiveScripts() {
            try {
                if (!Globals.Client.IsLoggedIn() || !_mainView.view.Visible) {
                    return;
                }

                UIScriptsList.ClearRows();

                var scripts = Globals.Client.GetScripts();

                foreach (var scriptID in scripts.Keys) {
                    // Green status icon (running):  0x060069A1
                    // White status icon (paused):   0x0600699E
                    // Red status icon (error?):     0x0600699F
                    // 
                    // Cancel icon:                  0x060011F8

                    var script = scripts[scriptID];
                    
                    HudList.HudListRowAccessor newRow = UIScriptsList.AddRow();
                    ((HudStaticText)newRow[0]).Text = script.GetName();
                    ((HudStaticText)newRow[1]).Text = script.GetFriendlyRuntime();
                    ((HudStaticText)newRow[1]).TextColor = System.Drawing.Color.Green;
                    ((HudPictureBox)newRow[2]).Image = 0x060069A1;
                    ((HudPictureBox)newRow[3]).Image = 0x060011F8;
                    ((HudStaticText)newRow[4]).Text = scriptID;
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private bool disposed;

        public void Dispose() {
            try {
                Dispose(true);

                GC.SuppressFinalize(this);
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        protected virtual void Dispose(bool disposing) {
            try {
                if (!disposed) {
                    if (disposing) {
                        ic.Dispose();
                        UILoadScriptDropdown.Dispose();
                        UIRefreshScripts.Dispose();
                        UILoadScript.Dispose();
                        UIScriptsList.Dispose();
                    }

                    disposed = true;
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private DateTime lastThought = DateTime.MinValue;

        public void Think() {
            try {
                if (DateTime.UtcNow - lastThought > TimeSpan.FromSeconds(1)) {
                    lastThought = DateTime.UtcNow;

                    if (_mainView != null && _mainView.view.Visible) {
                        RefreshActiveScripts();
                    }
                }
            }
            catch (Exception e) { Util.LogException(e); }
        }
    }
}