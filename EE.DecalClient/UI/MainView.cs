﻿using System;
using System.Globalization;

using VirindiViewService.Controls;
using EE.DecalClient.UI.Pages;

namespace EE.DecalClient.UI {
    class MainView : IDisposable {
        public readonly VirindiViewService.ViewProperties properties;
        public readonly VirindiViewService.ControlGroup controls;
        public readonly VirindiViewService.HudView view;

        public ScriptsPage scriptsPage;

        public MainView() {
            try {
                // Create the view
                VirindiViewService.XMLParsers.Decal3XMLParser parser = new VirindiViewService.XMLParsers.Decal3XMLParser();
                parser.ParseFromResource("EE.DecalClient.UI.mainView.xml", out properties, out controls);

                // Display the view
                view = new VirindiViewService.HudView(properties, controls);

                scriptsPage = new ScriptsPage(this);

            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private bool disposed;

        public void Dispose() {
            Dispose(true);

            // Use SupressFinalize in case a subclass
            // of this type implements a finalizer.
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing) {
            // If you need thread safety, use a lock around these 
            // operations, as well as in your methods that use the resource.
            if (!disposed) {
                if (disposing) {
                    // children
                    if (scriptsPage != null) scriptsPage.Dispose();

                    //Remove the view
                    if (view != null) view.Dispose();
                }

                // Indicate that the instance has been disposed.
                disposed = true;
            }
        }

        public void Think() {
            if (scriptsPage != null) {
                scriptsPage.Think();
            }
        }
    }
}
