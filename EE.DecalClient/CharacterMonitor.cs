﻿using Decal.Adapter.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EE.Services;

namespace EE.DecalClient {
    class CharacterMonitor {
        // character monitor update interal
        private static TimeSpan updateInterval = TimeSpan.FromMilliseconds(100);
        private static DateTime lastUpdate = DateTime.MinValue;

        public static EECharacter character;

        public CharacterMonitor() {

        }

        public static void Think() {
            if (DateTime.UtcNow - lastUpdate > updateInterval) {
                lastUpdate = DateTime.UtcNow;

                if (Globals.Core.CharacterFilter != null && Globals.Core.CharacterFilter.Id != 0) {
                    if (character == null) {
                        character = new EECharacter(Globals.Core.CharacterFilter);
                        
                        try {
                            Globals.Client.pipeProxy.OnChangeCharacter(character);
                        }
                        catch (Exception e) { }
                        return;
                    }

                    character.UpdatePropertiesFromCharacterFilter(Globals.Core.CharacterFilter);

                    if (character.IsDirty) {
                        try {
                            Globals.Client.pipeProxy.OnChangeCharacter(character);
                        }
                        catch (Exception e) { }

                        character.Clean();
                    }
                }
            }
        }
    }
}
