﻿using Decal.Adapter;
using Decal.Adapter.Wrappers;
using EE.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Configuration;
using System.Text;
using System.Threading;

namespace EE.DecalClient {
    // Define a class to hold custom event info
    public class ScriptStatusEventArgs : EventArgs {
        public ScriptStatusEventArgs(string sid, string scriptName) {
            SID = sid;
            ScriptName = scriptName;
        }

        private string sid;
        private string scriptName;

        public string SID {
            get { return sid; }
            set { sid = value; }
        }
        public string ScriptName {
            get { return scriptName; }
            set { scriptName = value; }
        }
    }


    /// <summary>
    /// Client controller. Acts as callback client.
    /// </summary>
    public class ClientController : IDecalServiceCallback {
        public event EventHandler<ScriptStatusEventArgs> ScriptStartedEvent;
        public event EventHandler<ScriptStatusEventArgs> ScriptStoppedEvent;

        public IDecalService pipeProxy;
        private ChannelFactory<IDecalService> pipeFactory;
        private bool connected = false;
        private bool shouldPong = false;

        private DateTime lastConnectionCheck = DateTime.MinValue;
        private bool isRegistered = false;
        private bool isLoggedIn = false;

        private Dictionary<string, Script> scripts = new Dictionary<string, Script>();
        public List<string> availableScripts = new List<string>();

        /// <summary>
        /// Creates a new instance of the <see cref="ClientController"/> class.
        /// </summary>
        public ClientController() {
        }

        public void Init() {
        }

        private void Connect() {
            try {
                if (pipeProxy != null && (((IClientChannel)pipeProxy).State == CommunicationState.Opened || ((IClientChannel)pipeProxy).State == CommunicationState.Opening)) {
                    return;
                }

                isRegistered = false;

                var binding = new NetNamedPipeBinding();

                binding.OpenTimeout = TimeSpan.FromSeconds(2);
                binding.SendTimeout = TimeSpan.FromSeconds(2);
                binding.ReceiveTimeout = TimeSpan.FromSeconds(2);

                pipeFactory = new DuplexChannelFactory<IDecalService>(
                        new InstanceContext(this),
                        binding,
                        "net.pipe://localhost/EEHostService");

                //create a communication channel and register for its events
                pipeProxy = pipeFactory.CreateChannel();
                ((IClientChannel)pipeProxy).Opened += PipeProxyOpened;

                try {
                    //try to open the connection
                    ((IClientChannel)pipeProxy).Open();
                }
                catch (Exception e) {
                    //Util.LogException(e);
                }
            }
            catch (Exception e) { Util.LogException(e); }
        }

        public void RegisterWithHost() {
            if (isRegistered == false && isLoggedIn == true) {
                if (IsPipeOpen()) {
                    pipeProxy.RegisterClient(Globals.Core.CharacterFilter.Server, Globals.Core.CharacterFilter.Name);
                }
            }
        }

        internal void StopScript(string scriptID) {
            try {
                pipeProxy.StopClientScript(scriptID);
            }
            catch (Exception e) { Util.LogException(e); }
        }

        void PipeProxyOpened(object sender, EventArgs e) {
            var proxy = sender as IClientChannel;

            Util.WriteToChat("Connected to EE.Host");

            connected = true;

            shouldPong = true;
            proxy.Faulted += PipeProxyFaulted;

            RegisterWithHost();
        }

        void PipeProxyFaulted(object sender, EventArgs e) {
            var proxy = sender as IClientChannel;

            if (connected == true) {
                Util.WriteToChat("Disconnected from EE.Host");
                connected = false;
                scripts.Clear();
            }

            if (proxy != null) {
                proxy.Faulted -= PipeProxyFaulted;
                proxy.Opened -= PipeProxyOpened;
                proxy = null;
            }

            this.pipeFactory = null;
        }

        internal bool IsLoggedIn() {
            return isLoggedIn;
        }

        internal void CharacterFilter_ChangeFellowship(object sender, ChangeFellowshipEventArgs e) {
            try {
                if (IsPipeOpen()) {
                    pipeProxy.OnChangeFellowship(new Services.EventArgs.ChangeFellowshipEventArgsWrapper(e));
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        internal void Current_ContainerOpened(object sender, ContainerOpenedEventArgs e) {
            try {
                if (IsPipeOpen()) {
                    pipeProxy.OnContainerOpened(new Services.EventArgs.ContainerOpenedEventArgsWrapper(e));
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        internal void Current_CommandLineText(object sender, ChatParserInterceptEventArgs e) {
            try {
                if (IsPipeOpen()) {
                    pipeProxy.OnCommandLineText(new Services.EventArgs.ChatParserInterceptEventArgsWrapper(e));
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        internal void Current_ChatNameClicked(object sender, ChatClickInterceptEventArgs e) {
            try {
                if (IsPipeOpen()) {
                    pipeProxy.OnChatNameClicked(new Services.EventArgs.ChatClickInterceptEventArgsWrapper(e));
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        internal void CharacterFilter_Login(object sender, LoginEventArgs e) {
            try {
                isLoggedIn = true;

                if (IsPipeOpen()) {
                    RegisterWithHost();
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        internal void CharacterFilter_LoginComplete(object sender, EventArgs e) {
            try {
                if (IsPipeOpen()) {
                    pipeProxy.OnLoginComplete();
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        internal void WorldFilter_CreateObject(object sender, CreateObjectEventArgs e) {
            try {
                if (IsPipeOpen()) {
                    if (e.New != null) {
                        // TODO: only send changed data?
                        EEWorldObject eewo = new EEWorldObject(e.New);

                        pipeProxy.OnCreateObject(new Services.EventArgs.CreateObjectEventArgsWrapper(eewo));
                    }
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        internal void WorldFilter_ChangeObject(object sender, ChangeObjectEventArgs e) {
            try {
                if (IsPipeOpen()) {
                    // TODO: only send changed data?
                    EEWorldObject eewo = new EEWorldObject(e.Changed);

                    pipeProxy.OnChangeObject(new Services.EventArgs.ChangeObjectEventArgsWrapper((int)e.Change, eewo));
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        internal void WorldFilter_ReleaseObject(object sender, ReleaseObjectEventArgs e) {
            try {
                if (IsPipeOpen()) {
                    // TODO: only send changed data?
                    EEWorldObject eewo = new EEWorldObject(e.Released);

                    pipeProxy.OnReleaseObject(new Services.EventArgs.ReleaseObjectEventArgsWrapper(eewo));
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        internal void Current_ChatBoxMessage(object sender, ChatTextInterceptEventArgs e) {
            try {
                if (IsPipeOpen()) {
                    pipeProxy.OnChatBoxMessage(new Services.EventArgs.ChatTextInterceptEventArgsWrapper(e));
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        internal void CharacterFilter_ChangeVital(object sender, ChangeVitalEventArgs e) {
            try {
                if (IsPipeOpen()) {
                    pipeProxy.OnChangeVital(new Services.EventArgs.ChangeVitalEventArgsWrapper(e));
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private bool IsPipeOpen() {
            return (pipeProxy != null && ((IClientChannel)pipeProxy).State == CommunicationState.Opened);

        }

        internal Dictionary<string, Script> GetScripts() {
            return scripts;
        }

        /// <summary>
        /// Invoked by the server, marks this client as registered
        /// </summary>
        /// <param name="uid">uid</param>
        public void Registered(string uid) {
            isRegistered = true;
        }

        public void Ping() {
            shouldPong = true;
        }

        public void Error(DecalServiceError error) {
            try {
                Globals.Host.Actions.AddChatText("[" + Globals.PluginName + "] " + error.text, 21);
            }
            catch (Exception e) { Util.LogException(e); }
        }

        public void RunScript(string scriptName) {
            try {
                if (IsPipeOpen()) {
                    pipeProxy.RunClientScript(scriptName);
                }
            }
            catch (Exception e) { Util.LogException(e); }
        }

        /// <summary>
        /// Invoked by the server, echos a message to the chat window
        /// </summary>
        /// <param name="message"></param>
        /// <param name="color"></param>
        public void AddChatText(string message, int color) {
            Globals.Host.Actions.AddChatText(message, color);
        }

        /// <summary>
        /// Invoked when host wants us to add a command to the chat box
        /// </summary>
        /// <param name="command"></param>
        public void InvokeChatParser(string command) {
            CoreManager.Current.Actions.InvokeChatParser(command);
        }

        /// <summary>
        /// Invoked when host has registered us
        /// </summary>
        /// <param name="sid"></param>
        /// <param name="scriptName"></param>
        public void NewScriptRunning(string sid, string scriptName) {
            try {
                AddChatText(String.Format("script is running: {0}.{1}", scriptName, sid), 11);

                var script = new Script(sid, scriptName);

                try {
                    this.scripts.Add(sid, script);
                }
                catch (Exception e) {
                    Util.LogException(e);
                }

                ScriptStartedEvent?.Invoke(this, new ScriptStatusEventArgs(sid, scriptName));
            }
            catch (Exception e) { Util.LogException(e); }
        }

        public void ScriptStopped(string sid, string scriptName) {
            try {
                AddChatText(String.Format("Script was stopped: {0}.{1}", scriptName, sid), 11);

                try {
                    this.scripts.Remove(sid);
                }
                catch (Exception e) { Util.LogException(e); }

                ScriptStoppedEvent?.Invoke(this, new ScriptStatusEventArgs(sid, scriptName));
            }
            catch (Exception e) { Util.LogException(e); }
        }

        public void AvailableScripts(List<string> availableScripts) {
            try {
                this.availableScripts = availableScripts;
            }
            catch (Exception e) { Util.LogException(e); }
        }

        public void Think() {
            try {
                if (DateTime.UtcNow - lastConnectionCheck > TimeSpan.FromSeconds(3)) {
                    lastConnectionCheck = DateTime.UtcNow;
                    if (pipeProxy == null || (((IClientChannel)pipeProxy).State != CommunicationState.Opened && ((IClientChannel)pipeProxy).State != CommunicationState.Opening)) {
                        Connect();
                    }
                }

                if (IsPipeOpen()) {
                    if (shouldPong) {
                        shouldPong = false;
                        pipeProxy.Pong();
                    }

                    CharacterMonitor.Think();
                }
            }
            catch (Exception e) { Util.LogException(e); }
        }

        internal void Shutdown() {
            try {
                if (IsPipeOpen() && isRegistered) {
                    pipeProxy.UnregisterClient();
                    isRegistered = false;
                }
            }
            catch (Exception e) {
                Util.LogException(e);
            }
        }

        internal void Dispose() {
            try {
                if (IsPipeOpen()) {
                    pipeProxy = null;
                    pipeFactory = null;
                }
            }
            catch (Exception e) { Util.LogException(e); }
        }
    }
}
