﻿using System;

using Decal.Adapter;
using Decal.Adapter.Wrappers;

namespace EE.DecalClient {
	public static class Globals
	{
		public static void Init(string pluginName, PluginHost host, CoreManager core, ClientController eeClient)
		{
			PluginName = pluginName;

			Host = host;

			Core = core;

            Client = eeClient;
		}

		public static string PluginName { get; private set; }

		public static PluginHost Host { get; private set; }

        public static CoreManager Core { get; private set; }

        public static ClientController Client { get; private set; }
    }
}
