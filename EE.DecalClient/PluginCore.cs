﻿using System;

using Decal.Adapter;
using Decal.Adapter.Wrappers;
using MyClasses.MetaViewWrappers;

using System.ServiceModel;
using EE.DecalClient.UI;

/*
 * Created by Mag-nus. 8/19/2011, VVS added by Virindi-Inquisitor.
 * 
 * No license applied, feel free to use as you wish. H4CK TH3 PL4N3T? TR45H1NG 0UR R1GHT5? Y0U D3C1D3!
 * 
 * Notice how I use try/catch on every function that is called or raised by decal (by base events or user initiated events like buttons, etc...).
 * This is very important. Don't crash out your users!
 * 
 * In 2.9.6.4+ Host and Core both have Actions objects in them. They are essentially the same thing.
 * You sould use Host.Actions though so that your code compiles against 2.9.6.0 (even though I reference 2.9.6.5 in this project)
 * 
 * If you add this plugin to decal and then also create another plugin off of this sample, you will need to change the guid in
 * Properties/AssemblyInfo.cs to have both plugins in decal at the same time.
 * 
 * If you have issues compiling, remove the Decal.Adapater and VirindiViewService references and add the ones you have locally.
 * Decal.Adapter should be in C:\Games\Decal 3.0\
 * VirindiViewService should be in C:\Games\VirindiPlugins\VirindiViewService\
*/

namespace EE.DecalClient
{
    //Attaches events from core
	[WireUpBaseEvents]

    //View (UI) handling
	[MVView("EE.DecalClient.mainView.xml")]
    [MVWireUpControlEvents]

	// FriendlyName is the name that will show up in the plugins list of the decal agent (the one in windows, not in-game)
	// View is the path to the xml file that contains info on how to draw our in-game plugin. The xml contains the name and icon our plugin shows in-game.
	// The view here is SamplePlugin.mainView.xml because our projects default namespace is SamplePlugin, and the file name is mainView.xml.
	// The other key here is that mainView.xml must be included as an embeded resource. If its not, your plugin will not show up in-game.
	[FriendlyName("EEClient")]
	public class PluginCore : PluginBase
	{
        int myId = 0;

        MainView mainView;
        ClientController eeClient;
        EchoFilter2 ef;

		/// <summary>
		/// This is called when the plugin is started up. This happens only once.
		/// </summary>
		protected override void Startup()
		{
			try {
                eeClient = new ClientController();
                eeClient.Init();

                // This initializes our static Globals class with references to the key objects your plugin will use, Host and Core.
                // The OOP way would be to pass Host and Core to your objects, but this is easier.
                Globals.Init("EEClient", Host, Core, eeClient);

                mainView = new MainView();

                CoreManager.Current.EchoFilter.ServerDispatch += new EventHandler<NetworkMessageEventArgs>(EchoFilter_ServerDispatch);

                //CoreManager.Current.RenderFrame += new EventHandler<EventArgs>(Current_RenderFrame);

                //CoreManager.Current.WorldFilter.ResetTrade += new EventHandler<ResetTradeEventArgs>(WorldFilter_ResetTrade);
                //CoreManager.Current.WorldFilter.MoveObject += new EventHandler<MoveObjectEventArgs>(WorldFilter_MoveObject);
                //CoreManager.Current.WorldFilter.FailToCompleteTrade += new EventHandler(WorldFilter_FailToCompleteTrade);
                //CoreManager.Current.WorldFilter.FailToAddTradeItem += new EventHandler<FailToAddTradeItemEventArgs>(WorldFilter_FailToAddTradeItem);
                //CoreManager.Current.WorldFilter.EnterTrade += new EventHandler<EnterTradeEventArgs>(WorldFilter_EnterTrade);
                //CoreManager.Current.WorldFilter.EndTrade += new EventHandler<EndTradeEventArgs>(WorldFilter_EndTrade);
                //CoreManager.Current.WorldFilter.DeclineTrade += new EventHandler<DeclineTradeEventArgs>(WorldFilter_DeclineTrade);
                CoreManager.Current.WorldFilter.ReleaseObject += new EventHandler<ReleaseObjectEventArgs>(eeClient.WorldFilter_ReleaseObject);
                CoreManager.Current.WorldFilter.CreateObject += new EventHandler<CreateObjectEventArgs>(eeClient.WorldFilter_CreateObject);
                CoreManager.Current.WorldFilter.ChangeObject += new EventHandler<ChangeObjectEventArgs>(eeClient.WorldFilter_ChangeObject);
                //CoreManager.Current.WorldFilter.ApproachVendor += new EventHandler<ApproachVendorEventArgs>(WorldFilter_ApproachVendor);
                //CoreManager.Current.WorldFilter.AddTradeItem += new EventHandler<AddTradeItemEventArgs>(WorldFilter_AddTradeItem);
                //CoreManager.Current.WorldFilter.AcceptTrade += new EventHandler<AcceptTradeEventArgs>(WorldFilter_AcceptTrade);

                //CoreManager.Current.CharacterFilter.StatusMessage += new EventHandler<StatusMessageEventArgs>(CharacterFilter_StatusMessage);
                //CoreManager.Current.CharacterFilter.SpellCast += new EventHandler<SpellCastEventArgs>(CharacterFilter_SpellCast);
                CoreManager.Current.CharacterFilter.LoginComplete += new EventHandler(eeClient.CharacterFilter_LoginComplete);
                //CoreManager.Current.CharacterFilter.Login += new EventHandler<LoginEventArgs>(eeClient.CharacterFilter_Login);
                //CoreManager.Current.CharacterFilter.Logoff += new EventHandler<LogoffEventArgs>(CharacterFilter_Logoff);
                //CoreManager.Current.CharacterFilter.Death += new EventHandler<DeathEventArgs>(CharacterFilter_Death);
                CoreManager.Current.CharacterFilter.ChangeVital += new EventHandler<ChangeVitalEventArgs>(eeClient.CharacterFilter_ChangeVital);
                //CoreManager.Current.CharacterFilter.ChangeExperience += new EventHandler<ChangeExperienceEventArgs>(CharacterFilter_ChangeExperience);
                //CoreManager.Current.CharacterFilter.ChangeEnchantments += new EventHandler<ChangeEnchantmentsEventArgs>(CharacterFilter_ChangeEnchantments);
                //CoreManager.Current.CharacterFilter.ActionComplete += new EventHandler(CharacterFilter_ActionComplete);
                

                //CoreManager.Current.EchoFilter.ServerDispatch += new EventHandler<NetworkMessageEventArgs>(EchoFilter_ServerDispatch);
                CoreManager.Current.CharacterFilter.ChangeFellowship += new EventHandler<ChangeFellowshipEventArgs>(eeClient.CharacterFilter_ChangeFellowship);

                CoreManager.Current.ContainerOpened += new EventHandler<ContainerOpenedEventArgs>(eeClient.Current_ContainerOpened);
                CoreManager.Current.CommandLineText += new EventHandler<ChatParserInterceptEventArgs>(eeClient.Current_CommandLineText);
                CoreManager.Current.ChatNameClicked += new EventHandler<ChatClickInterceptEventArgs>(eeClient.Current_ChatNameClicked);
                CoreManager.Current.ChatBoxMessage += new EventHandler<ChatTextInterceptEventArgs>(eeClient.Current_ChatBoxMessage);


                // internal stuff
                CoreManager.Current.RenderFrame += new EventHandler<EventArgs>(Current_RenderFrame);
                CoreManager.Current.CharacterFilter.Logoff += new EventHandler<LogoffEventArgs>(CharacterFilter_Logoff);
                CoreManager.Current.CharacterFilter.Login += new EventHandler<LoginEventArgs>(CharacterFilter_Login);
                CoreManager.Current.WorldFilter.CreateObject += new EventHandler<CreateObjectEventArgs>(WorldFilter_CreateObject);
            }
            catch (Exception ex) { Util.LogException(ex); }
		}

        private void EchoFilter_ServerDispatch(object sender, NetworkMessageEventArgs e) {
            try {
                return;
                /*
                switch (e.Message.Type) {
                    case 0xF7B0:
                        Util.WriteToChat("got game action event: " + ((int)e.Message["event"]).ToString("X"));
                        if ((int)e.Message["event"] == 0x0274) {
                            Util.WriteToChat("Got status: " + (string)e.Message["text"]);
                        }
                        break;
                }
                */
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void WorldFilter_CreateObject(object sender, CreateObjectEventArgs e) {
            try {
                if (myId == e.New.Id) {
                    CoreManager.Current.WorldFilter.CreateObject -= new EventHandler<CreateObjectEventArgs>(WorldFilter_CreateObject);
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void CharacterFilter_Login(object sender, LoginEventArgs e) {
            try {
                this.myId = e.Id;
                eeClient.CharacterFilter_Login(sender, e);
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void CharacterFilter_Logoff(object sender, LogoffEventArgs e) {
            try {
                if (eeClient != null) {
                    eeClient.Shutdown();
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        /// <summary>
        /// This is called when the plugin is shut down. This happens only once.
        /// </summary>
        protected override void Shutdown()
		{
			try {
                CoreManager.Current.WorldFilter.ReleaseObject -= new EventHandler<ReleaseObjectEventArgs>(eeClient.WorldFilter_ReleaseObject);
                CoreManager.Current.WorldFilter.CreateObject -= new EventHandler<CreateObjectEventArgs>(eeClient.WorldFilter_CreateObject);
                CoreManager.Current.WorldFilter.ChangeObject -= new EventHandler<ChangeObjectEventArgs>(eeClient.WorldFilter_ChangeObject);

                
                CoreManager.Current.CharacterFilter.ChangeVital -= new EventHandler<ChangeVitalEventArgs>(eeClient.CharacterFilter_ChangeVital);
                CoreManager.Current.CharacterFilter.LoginComplete -= new EventHandler(eeClient.CharacterFilter_LoginComplete);
                //CoreManager.Current.CharacterFilter.Login -= new EventHandler<LoginEventArgs>(eeClient.CharacterFilter_Login);
                CoreManager.Current.CharacterFilter.ChangeFellowship -= new EventHandler<ChangeFellowshipEventArgs>(eeClient.CharacterFilter_ChangeFellowship);

                CoreManager.Current.ContainerOpened -= new EventHandler<ContainerOpenedEventArgs>(eeClient.Current_ContainerOpened);
                CoreManager.Current.CommandLineText -= new EventHandler<ChatParserInterceptEventArgs>(eeClient.Current_CommandLineText);
                CoreManager.Current.ChatNameClicked -= new EventHandler<ChatClickInterceptEventArgs>(eeClient.Current_ChatNameClicked);
                CoreManager.Current.ChatBoxMessage -= new EventHandler<ChatTextInterceptEventArgs>(eeClient.Current_ChatBoxMessage);

                CoreManager.Current.RenderFrame -= new EventHandler<EventArgs>(Current_RenderFrame);
                CoreManager.Current.CharacterFilter.Logoff -= new EventHandler<LogoffEventArgs>(CharacterFilter_Logoff);
                CoreManager.Current.CharacterFilter.Login -= new EventHandler<LoginEventArgs>(CharacterFilter_Login);

                CoreManager.Current.EchoFilter.ServerDispatch -= new EventHandler<NetworkMessageEventArgs>(EchoFilter_ServerDispatch);

                //Destroy the view.
                MVWireupHelper.WireupEnd(this);

                if (eeClient != null) {
                    eeClient.Dispose();
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void Current_RenderFrame(object sender, EventArgs e) {
            try {
                Think();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void Think() {
            if (mainView != null) {
                mainView.Think();
            }
            if (eeClient != null) {
                eeClient.Think();
            }
        }
	}
}
